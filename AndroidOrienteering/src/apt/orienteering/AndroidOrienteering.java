package apt.orienteering;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class AndroidOrienteering extends Activity {
	// Helpful variables
	TrackHelper helper;
	Track track;
	
	private String[] fileList;
	private File path = new File(Environment.getExternalStorageDirectory() + "/AndroidOrienteering/Tracks/");

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Set layout to main layout
		setContentView(R.layout.main);
		
		// Set picture to title bar
		View title = getWindow().findViewById(android.R.id.title);
		View titleBar = (View)title.getParent();
		this.setTitle("");
		titleBar.setBackgroundColor(R.color.grey);
		titleBar.setBackgroundResource(R.drawable.nadpisao);

		// Button and listener for "Create new track"
		Button create = (Button) findViewById(R.id.create_track);
		create.setOnClickListener(onCreateTrack);

		// Button and listener for "Choose track"
		Button choose = (Button) findViewById(R.id.choose);
		choose.setOnClickListener(onChooseTrack);

		// Button and listener for "Done tracks"
		Button doneTracks = (Button) findViewById(R.id.done_tracks);
		doneTracks.setOnClickListener(onDoneTracks);
		
		// Button and listener for "Load track from XML"
		Button loadTrack = (Button) findViewById(R.id.load_track);
		loadTrack.setOnClickListener(onLoadTrack);

		// Button and listener for "Help"
		Button help = (Button) findViewById(R.id.help);
		help.setOnClickListener(onHelp);

		// Check and create directory for map files
		checkAndCreateDirectory("/AndroidOrienteering");
		
		helper = new TrackHelper(this);
		track = new Track();
	}
	
	@Override
	public void onDestroy(){
		super.onDestroy();
		helper.close();
	}

	/** Check and create directory for map files */
	public static void checkAndCreateDirectory(String dirName) {
		File newDir = new File(Environment.getExternalStorageDirectory() + dirName);
		// If directory doesn't exist, create it
		if (!newDir.exists()) {
			newDir.mkdirs();
		}
	}

	/** OnClick method for button "Create new track" */
	private View.OnClickListener onCreateTrack = new View.OnClickListener() {
		public void onClick(View v) {
			Intent i = new Intent(AndroidOrienteering.this, SelectionOfCountry.class);
			// Start new Activity
			startActivity(i);
		}
	};

	/** OnClick method for button "Choose track" 
	 * Start new Activity which displays a list of tracks
	 * */
	private View.OnClickListener onChooseTrack = new View.OnClickListener() {
		public void onClick(View v) {
			// Start new Activity with the list of existing tracks
			Intent i = new Intent(AndroidOrienteering.this, ChooseTrack.class);
			startActivity(i);
		}
	};

	/** OnClick method for button "Done tracks" 
	 * Start new Activity which displays a list of done tracks
	 * */
	private View.OnClickListener onDoneTracks = new View.OnClickListener() {
		public void onClick(View v) {
			// Start new Activity with the list of done tracks
			Intent i = new Intent(AndroidOrienteering.this, DoneTracks.class);
			startActivity(i);
		}
	};
	
	/** OnClick method for button "Load track from XML" */
	private View.OnClickListener onLoadTrack = new View.OnClickListener() {
		public void onClick(View v) {
			if (!path.exists()){
				Toast.makeText(AndroidOrienteering.this, "No directory with tracks!", Toast.LENGTH_LONG).show();
			} else {
				loadFileList();
				showFileDialog();
			}
		}
	};
	
	/** Function for loading list of XML files */
	private void loadFileList(){
		FilenameFilter filter = new FilenameFilter(){
			public boolean accept(File dir, String filename){
				return filename.contains(".xml"); 
			}
		};
		fileList = path.list(filter);		
	}
	
	/** Show dialog with XML files */
	private void showFileDialog() {
		AlertDialog.Builder builder = new Builder(this);
		builder.setTitle("Choose XML file");
		builder.setItems(fileList, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				try {
					boolean parse = parseXMLFile(fileList[which]);
					if (parse){
						saveTrackToDatabase();				
					} else {
						Toast.makeText(AndroidOrienteering.this, "Fault in XML file!", Toast.LENGTH_LONG).show();
					}
				} catch (XmlPullParserException e) {
					e.printStackTrace();
				}	
			}
		});
		builder.show();
	}
	
	/** Function for parsing XML file 
	 * @param file Name of XML file 
	 * @throws XmlPullParserException */
	private boolean parseXMLFile(String file) throws XmlPullParserException{
		XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
		factory.setNamespaceAware(true);
		XmlPullParser parser = factory.newPullParser();	

		try {
			File f = new File(Environment.getExternalStorageDirectory() + "/AndroidOrienteering/Tracks/" + file);
			FileInputStream fis = new FileInputStream(f);
			parser.setInput(new InputStreamReader(fis));
			
			int eventType = parser.getEventType();
			int p = 0;

			while (eventType != XmlPullParser.END_DOCUMENT){
				String name = null;
				
				switch(eventType){
				case XmlPullParser.START_TAG:
					name = parser.getName();
					if (name.equals("name")){
						track.setName(parser.getAttributeValue(0));
					} else if (name.equals("author")){
						track.setAuthor(parser.getAttributeValue(0));
					} else if (name.equals("controls")){
						track.setCountControls(Integer.parseInt(parser.getAttributeValue(0)));
					} else if (name.equals("length")){
						track.setLength(Integer.parseInt(parser.getAttributeValue(0)));
					} else if (name.equals("start")){
						track.setStart(Double.parseDouble(parser.getAttributeValue(0)), Double.parseDouble(parser.getAttributeValue(1)));						
					} else if (name.equals("finish")){
						track.setFinish(Double.parseDouble(parser.getAttributeValue(0)), Double.parseDouble(parser.getAttributeValue(1)));	
					} else if (name.equals("state")){
						track.setPath(parser.getAttributeValue(0));
					} else if (name.equals("checkpoints")){
						p = 0;
					} else if (name.equals("point")){
						track.setPoints(p, Double.parseDouble(parser.getAttributeValue(0)), Double.parseDouble(parser.getAttributeValue(1)));
						p++;
					} else {
						return false;
					}
				default:
					break;
				}
				
				eventType = parser.next();
			}
		} catch (FileNotFoundException e){
			
		} catch (IOException e){
			
		} catch (Exception e){
			
		}	
		return true;
	}
	
	/** Function for saving import track to database */
	private void saveTrackToDatabase(){		
		if (helper.getCountTrackName(track.getName()) != 0){ // Track with same name already exists
			Toast.makeText(AndroidOrienteering.this, "Track with same name already exists!", Toast.LENGTH_LONG).show();
		} else {
			// Save track
			helper.insertTrack(track.getName(), track.getAuthor(), track.getCountControls(), track.getLength(), track.getStart(), track.getFinish(), track.getPath());
			int id = helper.getTrackId(track.getName());
			// Save checkpoints
			for (int j = 0; j < track.getCountControls(); j++) {
				helper.insertCheckPoint(id, track.getPoint(j));
			}		
			Toast.makeText(AndroidOrienteering.this, "Track was successfully created!", Toast.LENGTH_LONG).show();
		}
	}

	/** OnClick method for button "Help" */
	private View.OnClickListener onHelp = new View.OnClickListener() {
		public void onClick(View v) {
			showHelpDialog();
		}
	};

	/** Show the help dialog */
	private void showHelpDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.title_help).setIcon(R.drawable.aohelp)
				.setMessage(R.string.help).setPositiveButton("OK", null).show();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		new MenuInflater(this).inflate(R.menu.option, menu);
		return (super.onCreateOptionsMenu(menu));
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Option EXIT - to finish application (this activity)
		if (item.getItemId() == R.id.exit) {
			AndroidOrienteering.this.finish();
			return (true);
		}		
		return (super.onOptionsItemSelected(item));
	}
}