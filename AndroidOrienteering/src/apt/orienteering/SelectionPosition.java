package apt.orienteering;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SelectionPosition extends Activity {
	// Variables for fields "EditText" in selection_position
	EditText latitudeText = null;
	EditText longitudeText = null;

	// Location Manager
	LocationManager locMgr = null;

	// Helping variables fileName and path
	String fileName = null;
	String path = null;
	private int length = 0;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Information from last activity
		Intent intent = getIntent();
		fileName = intent.getStringExtra("fileName");
		path = intent.getStringExtra("path");
		length = intent.getIntExtra("length", 0);

		// Set layout
		setContentView(R.layout.selection_position);

		// Create button myLocation for button "My Location" and clickListener for it
		Button myLocation = (Button) findViewById(R.id.my_location);
		myLocation.setOnClickListener(onMyLocation);

		// Create button nextCreate for button NEXT and clickListener for it
		Button nextCreate = (Button) findViewById(R.id.next_create);
		nextCreate.setOnClickListener(onNextCreate);

		// Set variables
		latitudeText = (EditText) findViewById(R.id.lat);
		longitudeText = (EditText) findViewById(R.id.lon);

		// Set location manager
		locMgr = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
	}
	
	@Override
	public void onDestroy(){
		super.onDestroy();
		locMgr.removeUpdates(onLocationChange);
	}

	/** OnClick method for button "My Location" */
	private View.OnClickListener onMyLocation = new View.OnClickListener() {
		public void onClick(View v) {
			Toast.makeText(SelectionPosition.this, "Please wait for finding of current location", Toast.LENGTH_SHORT)
				.show();
			
			// Last known location
			Location location = locMgr.getLastKnownLocation(LocationManager.GPS_PROVIDER);
			
			if (location!=null){
				double lat = location.getLatitude();
				double lon = location.getLongitude();
				startNewActivity(lat, lon);
			} else {
				if (Game.checkInternetConnection((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))){
					location = locMgr.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
					if (location != null){
						double lat = location.getLatitude();
						double lon = location.getLongitude();
						startNewActivity(lat, lon);
					} else {
						locMgr.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, onLocationChange);
					}
				} else {
					locMgr.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, onLocationChange);
				}
			}
		}
	};

	/** OnLocationChanged method for using button my location */
	LocationListener onLocationChange = new LocationListener() {
		public void onLocationChanged(Location fix) {
			// Get latitude and longitude from GPS
			double lat = fix.getLatitude();
			double lon = fix.getLongitude();
			// Stop to read change of location
			locMgr.removeUpdates(onLocationChange);
			// Start next Activity
			startNewActivity(lat, lon);
		}

		public void onProviderDisabled(String provider) {
			Toast.makeText(SelectionPosition.this, "GPS disabled",
					Toast.LENGTH_LONG).show();
		}

		public void onProviderEnabled(String provider) {
			Toast.makeText(SelectionPosition.this, "GPS enable",
					Toast.LENGTH_LONG).show();
		}

		public void onStatusChanged(String provider, int status, Bundle extras) {

		}
	};

	/** OnClick method for button "NEXT" in selection_position */
	private View.OnClickListener onNextCreate = new View.OnClickListener() {
		public void onClick(View v) {
			// Control the values in EditText boxes
			if (!latitudeText.getText().toString().equals("") && !longitudeText.getText().toString().equals("")) {
				// Parse latitude and longitude from EditText to double
				double lat = Double.parseDouble(latitudeText.getText().toString());
				double lon = Double.parseDouble(longitudeText.getText().toString());
				
				// Stop to read change of location if the button "My Location" was pressed
				locMgr.removeUpdates(onLocationChange);
				// Start next activity
				startNewActivity(lat, lon);
			} else { // No enter latitude or longitude => show AlertDialog
				showAlertDialog();
			}
		}
	};

	/** Show Alert Dialog */
	private void showAlertDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.noLatLonPlace)
				.setMessage("Please enter latitude and longitude in compulsory format!")
				.setPositiveButton("OK", null)
				.show();
	}

	/** Start new Activity and do needful things for it */
	private void startNewActivity(double lat, double lon) {
		// Prepare new Activity for start - map with latitude and longitude
		Intent i = new Intent(SelectionPosition.this, CreateTrack.class);
		i.putExtra("latitude", lat);
		i.putExtra("longitude", lon);
		i.putExtra("fileName", fileName);
		i.putExtra("path", path);
		i.putExtra("length", length);

		SelectionPosition.this.startActivity(i);
	}
}