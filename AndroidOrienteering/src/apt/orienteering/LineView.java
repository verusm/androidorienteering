package apt.orienteering;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

public class LineView extends View {
	/** Constructors */
	public LineView(Context context) {
		super(context);
		initLineView();
	}

	public LineView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initLineView();
	}

	public LineView(Context context, AttributeSet attrs, int defaultStyle) {
		super(context, attrs, defaultStyle);
		initLineView();
	}
	
	/** Objects for drawing to overlay */
	private Paint blueline;
	private Paint redline;
	private Paint text;
	
	// Helpful variables for data from sensors
	private float xAxis;
	private float zAxis;
	// Bearing to next control
	private float bearing;
	// Angle which camera is can displays on screen
	private float angle;
	
	/** Initialization of line view - call it in each constructor */
	protected void initLineView(){		
		setFocusable(true);		
		Resources r = this.getResources();
		
		// Define Paints for lines and text
		blueline = new Paint(Paint.ANTI_ALIAS_FLAG);
		blueline.setColor(r.getColor(R.color.blue));
		blueline.setStrokeWidth(5);
		
		redline = new Paint(Paint.ANTI_ALIAS_FLAG);
		redline.setColor(r.getColor(R.color.red));
		redline.setStrokeWidth(5);
			
		text = new Paint(Paint.ANTI_ALIAS_FLAG);
		text.setColor(r.getColor(R.color.blue));
		text.setTextSize(35);
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int measuredWidth = measure(widthMeasureSpec);
		int measuredHeight = measure(heightMeasureSpec);

		setMeasuredDimension(measuredWidth, measuredHeight);
	}

	/** Find out size from parent object */
	private int measure(int measSpec) {
		int result = 0;

		int specMode = MeasureSpec.getMode(measSpec);
		int specSize = MeasureSpec.getSize(measSpec);

		if (specMode == MeasureSpec.UNSPECIFIED) {
			// Size is not specified - set 200
			result = 200;
		} else {
			result = specSize;
		}
		return result;
	}
	
	/** Set xAxis to data from sensor */
	public void setX(float x){
		xAxis = x;
	}
	/** Set zAxis to data from sensor */
	public void setZ(float z){
		zAxis = z;
	}
	/** Set bearing to data from sensor */
	public void setBearing(float b){
		bearing = b;
	}
	/** Set angle to data from sensor */
	public void setAngle(double a){
		angle = (float) a;
	}
	
	/** Draw the line and text by using prepared objects */
	@Override
	protected void onDraw(Canvas canvas) {		
		// Helpful variable for position of line on screen
		float w = 0;
		// Helpful variable for calculation and convert of bearing
		float b = bearing + 180;
		if (b > 180){
			b = b - 180;
		}
		
		
		// The line will be display only in horizontal position of device (-55� do -90� horizontal orientation)
		if (xAxis > -90 && xAxis < -55){			
			// Convert to same way of use like zAxis (-180 to 180)
			if (bearing > 180){
				bearing = bearing - 360;
			}
			
			float difference = 0;
			// Count difference between zAxis and bearing
			if ((zAxis < 0 && bearing < 0) || (zAxis > 0 && bearing >0)){
				difference = Math.abs(zAxis - bearing);
			} else { // The values are from difference part of circle (one is from interval 0-180 other from interval -180-0)
				difference = Math.abs(zAxis) + Math.abs(bearing);
				if (difference > 180){
					difference = 360 - difference;
				}
			}
			
			if (difference > angle/2){	// The line is not display on screen
				// Display arrows, on which side the line is				
				if (bearing < 0){ 
					if ((zAxis > bearing && zAxis < 0) || (zAxis < b && zAxis > 0)){
						canvas.drawLine(0, 0, 0, getMeasuredHeight(), redline);
						canvas.drawLine(10, 10, 50, 10, redline);
						canvas.drawLine(10, 10, 30, 0, redline);
						canvas.drawLine(10, 10, 30, 20, redline);
						canvas.restore();
					} else {
						canvas.drawLine(getMeasuredWidth(), 0, getMeasuredWidth(), getMeasuredHeight(), redline);
						canvas.drawLine(getMeasuredWidth() - 10, 10, getMeasuredWidth() - 50, 10, redline);
						canvas.drawLine(getMeasuredWidth() - 10, 10, getMeasuredWidth() - 30, 0, redline);
						canvas.drawLine(getMeasuredWidth() - 10, 10, getMeasuredWidth() - 30, 20, redline);
						canvas.restore();
					}
				}
				else {
					if ((zAxis > bearing && zAxis < 0) || (zAxis < b && zAxis > 0)){
						canvas.drawLine(getMeasuredWidth(), 0, getMeasuredWidth(), getMeasuredHeight(), redline);
						canvas.drawLine(getMeasuredWidth() - 10, 10, getMeasuredWidth() - 50, 10, redline);
						canvas.drawLine(getMeasuredWidth() - 10, 10, getMeasuredWidth() - 30, 0, redline);
						canvas.drawLine(getMeasuredWidth() - 10, 10, getMeasuredWidth() - 30, 20, redline);
						canvas.restore();
					} else {
						canvas.drawLine(0, 0, 0, getMeasuredHeight(), redline);
						canvas.drawLine(10, 10, 50, 10, redline);
						canvas.drawLine(10, 10, 30, 0, redline);
						canvas.drawLine(10, 10, 30, 20, redline);
						canvas.restore();						
					}
				}
			} else { // The line is on the screen
				// zAxis - bearing = difference between value from sensor and bearing to control
				// Count where the line will be display on the screen
				w = getMeasuredWidth()/2 - (getMeasuredWidth()/angle * (zAxis - bearing));
			
				if (w == 0 || w == getMeasuredWidth()){ // The line is on the edge of screen
					canvas.drawLine(w, 0, w, getMeasuredHeight(), redline);
				} else { // Line on the screen
					canvas.drawLine(w, 0, w, getMeasuredHeight(), blueline);
				}
				canvas.restore();
			}
		} else { // Device is not in vertical position
			canvas.drawText("Rotate device to vertical position!", 5, 40, text);
			canvas.restore();
		}
	}
}