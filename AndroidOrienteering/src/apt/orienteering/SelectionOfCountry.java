package apt.orienteering;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RemoteViews;
import android.widget.Toast;
import apt.orienteering.AndroidOrienteering;

public class SelectionOfCountry extends Activity {
	// Arrays for list views
	private String[] countries = null;
	private String[] c_germany = null;
	private String[] c_france = null;
	private String[] c_great_britain = null;

	// Helpful variables
	private String fileName = null;
	private String path = null;
	private int length = 0;
	private File downloadFile = null;

	// Progress dialog for downloading
	private ProgressDialog progDialog;

	// Constant
	final File ROOT_DIR = Environment.getExternalStorageDirectory();

	// URL for maps
	public String downloadURL = "http://download.mapsforge.org/maps/europe/";

	// Helpful variables for notification for download file
	Notification notification = null;
	NotificationManager notificationManager = null;
	private int prog = 0;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Set view for new - ListView for choosing country of track
		setContentView(R.layout.choose_state);

		// Set arrays from XML
		countries = getResources().getStringArray(R.array.countries);
		c_germany = getResources().getStringArray(R.array.germany_countries);
		c_france = getResources().getStringArray(R.array.france_countries);
		c_great_britain = getResources().getStringArray(R.array.gb_countries);

		// Set ListView with countries
		ListView list = (ListView) findViewById(R.id.items);
		list.setAdapter(new ArrayAdapter<String>(SelectionOfCountry.this, R.layout.item_state, countries));
		list.setOnItemClickListener(onListClick);
	}

	/** The method for on click on item in ListView */
	private AdapterView.OnItemClickListener onListClick = new AdapterView.OnItemClickListener() {
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			// Find which country was chosen
			String c = countries[position];
			path = ROOT_DIR.toString() + "/AndroidOrienteering/";

			// Transfer name to lower case
			c = c.toLowerCase();
			// Change space to "_" in file name
			c = c.replace(" ", "_");

			if (c.equals("germany")) { // For Germany choose only state of Germany
				// Check directory Germany
				AndroidOrienteering.checkAndCreateDirectory("/AndroidOrienteering/germany");
				// Change path and downloadURL
				path = ROOT_DIR.toString() + "/AndroidOrienteering/germany/";
				downloadURL = downloadURL + "germany/";
				showGermanyDialog();
			} else if (c.equals("france")) { // For France choose only part of France
				// Check directory Germany
				AndroidOrienteering.checkAndCreateDirectory("/AndroidOrienteering/france");
				// Change path and downloadURL
				path = ROOT_DIR.toString() + "/AndroidOrienteering/france/";
				downloadURL = downloadURL + "france/";
				showFranceDialog();
			} else if (c.equals("great_britain")) { // For Great Britain choose only part of GB
				// Check directory Germany
				AndroidOrienteering.checkAndCreateDirectory("/AndroidOrienteering/great_britain");
				// Change path and downloadURL
				path = ROOT_DIR.toString() + "/AndroidOrienteering/great_britain/";
				downloadURL = downloadURL + "great_britain/";
				// Show dialog
				showGBDialog();
			} else { // Other countries
				fileName = c + ".map";
				// Check if file exists
				testFile();
			}
		}
	};

	/** The method for showing the dialog for choose state of Great Britain */
	private void showGBDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Choose part of Great Britain")
				.setItems(c_great_britain, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int item) {
						String c = c_great_britain[item];
						// Transfer name to lower case
						c = c.toLowerCase();
						// Change space to "_" - for name file
						c = c.replace(" ", "_");
						fileName = c + ".map";
						// Continue with control file
						testFile();
					}
				}).show();
	}

	/** The method for showing the dialog for choose state of France */
	private void showFranceDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Choose state of France")
				.setItems(c_france, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int item) {
						String c = c_france[item];
						// Transfer name to lower case
						c = c.toLowerCase();
						// Change space to "_" - for name file
						c = c.replace(" ", "_");
						fileName = c + ".map";
						// Continue with control file
						testFile();
					}
				}).show();
	}

	/** The method for showing the dialog for choose state of Germany */
	private void showGermanyDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Choose state of Germany")
				.setItems(c_germany, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int item) {
						String c = c_germany[item];
						// Transfer name to lower case
						c = c.toLowerCase();
						// Change space to "_" - for name file
						c = c.replace(" ", "_");
						// Change � to ue
						c = c.replace("�", "ue");
						fileName = c + ".map";
						// Continue with control file
						testFile();
					}
				}).show();
	}

	/** The function for control of existed map file */
	private void testFile() {		
		// Create file
		File file = new File(path, fileName);
		// File don't exist, download file
		if (!file.exists()) {
			// Show dialog
			showDownloadDialog();
			// TODO - Check if file was started to download before, but it was not successful
			// (file downcountry.map exists)
		} else { // File exists - display option for choosing part of map
			// Set content view for my location or set latitude/longitude
			selectPosition();
		}
	}

	/** The function for selection of map part, where the track will be created */
	private void selectPosition() {
		Intent i = new Intent(SelectionOfCountry.this, SelectionPosition.class);
		i.putExtra("fileName", fileName);
		i.putExtra("path", path);
		i.putExtra("length", length);
		startActivity(i);
	}

	/** The method for showing the dialog for downloading map */
	private void showDownloadDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("The map file is not available in the device. You can download the file or use only online mode.")
				.setPositiveButton("Download file", onDownload)
				.setNegativeButton("Use online mode", onOnlineMode)
				.show();
	}

	/** The method for on click "use online mode" button */
	private DialogInterface.OnClickListener onOnlineMode = new DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog, int id) {
			if (!Game.checkInternetConnection((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
				Toast.makeText(SelectionOfCountry.this,
						"No Internet connectivity", Toast.LENGTH_LONG).show();
			} else {
				selectPosition();
			}
		}
	};
	
	/** The method for on click download button */
	private DialogInterface.OnClickListener onDownload = new DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog, int id) {
			if (!Game.checkInternetConnection((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
				Toast.makeText(SelectionOfCountry.this, "No Internet connection", Toast.LENGTH_LONG).show();
			} else {	
				downloadFile = new File(path, "down" + fileName);
				// Call method for download file
				new DownloadFileAsync().execute(downloadURL + fileName);
				selectPosition();
			}
		}
	};

	/** PROGRESS DIALOG */
	class DownloadFileAsync extends AsyncTask<String, String, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Create notification and notificationManager
			notification = new Notification(R.drawable.ic_menu_download_icon, "Downloading of a map file", System.currentTimeMillis());

			Intent intent = new Intent(getApplicationContext(), SelectionOfCountry.class);
			final PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, 0);

			notification.flags = notification.flags|Notification.FLAG_ONGOING_EVENT;
			notification.contentView = new RemoteViews(getApplicationContext()
					.getPackageName(), R.layout.download_progress);
			notification.contentIntent = pendingIntent;
			notification.contentView.setImageViewResource(R.id.status_icon, R.drawable.ic_menu_download);
			notification.contentView.setTextViewText(R.id.status_text, "Downloading map file");
			notification.contentView.setProgressBar(R.id.status_progress, 100, prog, false);
			getApplicationContext();
			notificationManager = (NotificationManager) getApplicationContext()
					.getSystemService(Context.NOTIFICATION_SERVICE);
			notificationManager.notify(42, notification);
		}

		@Override
		protected String doInBackground(String... sUrl) {
			try {
				// Download file
				URL url = new URL(downloadURL + fileName);
				HttpURLConnection c = (HttpURLConnection) url.openConnection();
				c.setRequestMethod("GET");
				c.setDoOutput(true);
				c.connect();

				// Length of downloading file
				length = c.getContentLength();

				// File output stream
				FileOutputStream out = new FileOutputStream(downloadFile);
				// Input stream
				InputStream in = c.getInputStream();

				// Download code
				byte[] buffer = new byte[1024];
				int len = 0;

				while ((len = in.read(buffer)) > 0) {
					prog += len;
					notification.contentView.setProgressBar(R.id.status_progress, length, prog, false);
					notificationManager.notify(42, notification);
					out.write(buffer, 0, len);
				}
				// Close file
				out.close();
			} catch (Exception e) {
				Log.e("Error doInBackground",e.getMessage());
			}
			return null;
		}

		@Override
		protected void onPostExecute(String unused) {
			notificationManager.cancel(42);
			// Rename file after it is all download
			if (!downloadFile.renameTo(new File(path, fileName))){
				Log.e("Error rename file", "rename");
			}
		}
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case 0:
			progDialog = new ProgressDialog(this);
			progDialog.setMessage("Downloading file " + fileName);
			progDialog.setIndeterminate(false);
			progDialog.setMax(100);
			progDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			progDialog.setCancelable(true);
			progDialog.show();
			return progDialog;
		default:
			return null;
		}
	}
}