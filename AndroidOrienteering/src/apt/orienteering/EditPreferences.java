package apt.orienteering;

import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.PreferenceActivity;

public class EditPreferences extends PreferenceActivity {
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Load the preferences from an XML resource
		addPreferencesFromResource(R.xml.preferences);
		
		// Create preferences
		Preference pref = findPreference("current_location");
		Preference prefDistance = findPreference("cl_distance");
		
		// Set enabled prefDistance if current_location is checked
		if (pref.getSharedPreferences().getBoolean("current_location", false)){
			prefDistance.setEnabled(true);
		} else {			
			prefDistance.setEnabled(false);
		}
	
		// Set listener to current_location preference
		pref.setOnPreferenceChangeListener(prefChange);
	}
	
	private OnPreferenceChangeListener prefChange = new OnPreferenceChangeListener(){
		public boolean onPreferenceChange(Preference preference, Object object){
			Preference prefCurrentLocation = findPreference("current_location");
			Preference prefDistance = findPreference("cl_distance");
			
			boolean setCurrentLocation = prefCurrentLocation.getSharedPreferences().getBoolean("current_location", false);
			
			// Set enabled prefDistance if current_location is checked
			if (setCurrentLocation){
				prefDistance.setEnabled(false);
			} else {			
				prefDistance.setEnabled(true);
			}
			return (true);
		}
	};
}