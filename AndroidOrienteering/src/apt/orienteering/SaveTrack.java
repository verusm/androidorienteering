package apt.orienteering;

import org.mapsforge.core.GeoPoint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.database.Cursor;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class SaveTrack extends Activity {
	// Helpful variable
	private int countControl = 0;
	private int len = 0;
	private String state = null;
	// Variables for points
	private GeoPoint[] points = new GeoPoint[30];
	private GeoPoint start = null;
	private GeoPoint finish = null;
	// Helper for usage of database
	TrackHelper helper = null;
	Cursor model = null;

	private EditText name = null;
	private EditText author = null;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Set content view
		setContentView(R.layout.save_track);

		Intent i = getIntent();
		// Information from last activity
		countControl = i.getIntExtra("countControl", 0);
		state = i.getStringExtra("state");
		start = new GeoPoint(i.getDoubleExtra("startLat", 0.0), i.getDoubleExtra("startLon", 0.0));
		finish = new GeoPoint(i.getDoubleExtra("finishLat", 0.0), i.getDoubleExtra("finishLon", 0.0));
		// Set field of controls
		for (int j = 0; j < countControl; j++) {
			points[j] = new GeoPoint(i.getDoubleExtra("controlLat" + j, 0.0), i.getDoubleExtra("controlLon" + j, 0.0));
		}

		// Count length of whole track
		len = (int) calculationOfLength();

		// Create text views for name and author of the track
		name = (EditText)findViewById(R.id.name_track);
		author = (EditText)findViewById(R.id.author_track);

		// Create button save for button SAVE and OnClickListener for it
		Button save = (Button) findViewById(R.id.save_track);
		save.setOnClickListener(onSave);

		// Set TextViews with count of controls and length
		TextView count = (TextView)findViewById(R.id.count_of_control);
		count.setText(String.valueOf(countControl));
		TextView length = (TextView)findViewById(R.id.length);
		length.setText(String.valueOf(len) + " m");

		// Set helper for usage of database
		helper = new TrackHelper(this);
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		helper.close();
	}

	/** The method for on click on the button save */
	private View.OnClickListener onSave = new View.OnClickListener() {
		@Override
		public void onClick(View v) {			
			if (name.getText().toString().equals("")){
				// No string in name of track
				showAlertDialog();
			} else if (author.getText().toString().equals("")){
				// No string in field with name of author
				showAlertDialogAuthor();
			} else if (helper.getCountTrackName(name.getText().toString()) != 0){
				// Inserted name is same like already exited track
				showAlertDialogTrackName();
			} else { // Save the track to the database
				helper.insertTrack(name.getText().toString(), author.getText()
						.toString(), countControl, len, start, finish, state);

				int id = 0;
				id = helper.getTrackId(name.getText().toString());

				for (int j = 0; j < countControl; j++) {
					helper.insertCheckPoint(id, points[j]);
				}
					
				Intent intent = new Intent(SaveTrack.this, AndroidOrienteering.class);
				SaveTrack.this.finish();
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				// Go back to main menu
				startActivity(intent);
			}
		}
	};
	
	/** The method for showing Alert Dialog - no name of the track*/
	private void showAlertDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.noname)
				.setMessage("Please enter a name of the track!")
				.setPositiveButton("OK", null).show();
	}
	
	/** The method for showing Alert Dialog - no author name*/
	private void showAlertDialogAuthor() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("No name of author")
				.setMessage("Please enter a name of the author of the track!")
				.setPositiveButton("OK", null).show();
	}
	
	/** The method for showing Alert Dialog - same track name*/
	private void showAlertDialogTrackName() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.track_name)
				.setMessage("Track name already exists. Please enter another name of the track!")
				.setPositiveButton("OK", null).show();
	}

	/** The function for calculating of the length of the track */
	private double calculationOfLength() {
		double len = 0;

		// From start to 1. control
		Location loc = new Location("");
		loc.setLatitude(start.getLatitude());
		loc.setLongitude(start.getLongitude());

		Location loc2 = new Location("");
		loc2.setLatitude(points[0].getLatitude());
		loc2.setLongitude(points[0].getLongitude());

		len = loc.distanceTo(loc2);

		// Between controls
		for (int j = 0; j < countControl - 1; j++) {
			loc.setLatitude(points[j].getLatitude());
			loc.setLongitude(points[j].getLongitude());
			loc2.setLatitude(points[j + 1].getLatitude());
			loc2.setLongitude(points[j + 1].getLongitude());

			len += loc.distanceTo(loc2);
		}

		// From last control to finish
		loc.setLatitude(finish.getLatitude());
		loc.setLongitude(finish.getLongitude());

		len += loc.distanceTo(loc2);
		return len;
	}
}