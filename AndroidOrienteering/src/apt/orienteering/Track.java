package apt.orienteering;

import org.mapsforge.android.maps.overlay.ArrayCircleOverlay;
import org.mapsforge.android.maps.overlay.ArrayWayOverlay;
import org.mapsforge.android.maps.overlay.OverlayCircle;
import org.mapsforge.android.maps.overlay.OverlayWay;
import org.mapsforge.core.GeoPoint;
import android.graphics.Color;
import android.graphics.Paint;

public class Track {
	// Track's attributes
	private int id = 0;
	private String name = null;
	private String author = null;
	private int countControl = 0;
	private int length = 0;
	private GeoPoint start = null;
	private GeoPoint finish = null;
	private GeoPoint[] points = null;
	private String path = null;

	ArrayCircleOverlay circleOverlay = null;
	ArrayWayOverlay wayOverlay = null;

	private OverlayCircle[] helpCircle = new OverlayCircle[33];
	private OverlayWay[] helpWay = new OverlayWay[32];

	/** Constructor. */
	public Track() {
		// Set attributes for circle
		Paint circlePaintOutline = new Paint(Paint.ANTI_ALIAS_FLAG);
		circlePaintOutline.setStyle(Paint.Style.STROKE);
		circlePaintOutline.setColor(Color.RED);
		circlePaintOutline.setAlpha(128);
		circlePaintOutline.setStrokeWidth(3);

		circleOverlay = new ArrayCircleOverlay(null, circlePaintOutline);

		// Set attributes for way between points
		Paint wayPaintOutline = new Paint(Paint.ANTI_ALIAS_FLAG);
		wayPaintOutline.setStyle(Paint.Style.STROKE);
		wayPaintOutline.setColor(Color.RED);
		wayPaintOutline.setAlpha(128);
		wayPaintOutline.setStrokeWidth(3);
		wayPaintOutline.setStrokeJoin(Paint.Join.ROUND); // BEVEL/MITER

		wayOverlay = new ArrayWayOverlay(null, wayPaintOutline);

		points = new GeoPoint[30];
	}

	/**
	 * Function setId sets track's id
	 * @param trackId Track ID
	 */
	public void setId(int trackId) {
		id = trackId;
	}

	/**
	 * Function getId returns track's id
	 * @return Track's ID
	 */
	public int getId() {
		return id;
	}

	/** 
	 * Function setName sets track's name
	 * @param trackName Track's name
	 */
	public void setName(String trackName) {
		name = trackName;
	}

	/**
	 * Function getName returns track's name
	 * @return Track's name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Function setAuthor sets author's name of track
	 * @param trackAuthor Name of track's author
	 */
	public void setAuthor(String trackAuthor) {
		author = trackAuthor;
	}

	/**
	 * Function getAuthor returns author name
	 * @return Name of track's author
	 */
	public String getAuthor() {
		return author;
	}
	
	/**
	 * Function setLength sets length of track
	 * @param len Length of track
	 */
	public void setLength(int len){
		length = len;
	}
	
	/**
	 * Function getLength returns length of track
	 * @return Length of track
	 */
	public int getLength(){
		return length;
	}

	/**
	 * Function setPath sets path to file with map for the track
	 * @param trackPath Path to the map file
	 */
	public void setPath(String trackPath) {
		path = trackPath;
	}

	/**
	 * Function getPath returns path to file with map for this track
	 * @return Path to map file
	 */
	public String getPath() {
		return path;
	}

	/**
	 * Function setStart sets start point of track
	 * @param lat Latitude of start point
	 * @param lon Longitude of start point
	 */
	public void setStart(double lat, double lon) {
		start = new GeoPoint(lat, lon);
	}

	/**
	 * Function getStart returns start as GeoPoint
	 * @return Start point
	 */
	public GeoPoint getStart() {
		return start;
	}

	/**
	 * Function setFinish sets finish point of track
	 * @param lat Latitude of finish point
	 * @param lon Longitude of finish point
	 */
	public void setFinish(double lat, double lon) {
		finish = new GeoPoint(lat, lon);
	}

	/**
	 * Function getFinish returns finish point as GeoPoint
	 * @return Finish point
	 */
	public GeoPoint getFinish() {
		return finish;
	}

	/**
	 * Function setCountControls set count of control points in track
	 * @param count Count of control points
	 */
	public void setCountControls(int count) {
		countControl = count;

	}

	/**
	 * Function getCountControls returns count of control points in the track
	 * @return Count of points
	 */
	public int getCountControls() {
		return countControl;
	}

	/**
	 * Function setPoints sets point in track
	 * @param j Index of control point
	 * @param lat Latitude of point
	 * @param lon Longitude of point
	 */
	public void setPoints(int j, double lat, double lon) {
		points[j] = new GeoPoint(lat, lon);
	}

	/**
	 * Function addPoint adds point to track
	 * @param j Index of control point
	 * @param point Point
	 */
	public void addPoint(int j, GeoPoint point) {
		points[j] = point;
	}

	/**
	 * Function getPoint returns chosen point from track
	 * @param j Index of control point
	 * @return Control point
	 */
	public GeoPoint getPoint(int j) {
		return points[j];
	}

	/**
	 * Function getWayOverlay returns overlay with ways between controls
	 * @return Overlay
	 */
	public ArrayWayOverlay getWayOverlay() {
		return wayOverlay;
	}

	/**
	 * Function getCircleOverlay returns overlay with control points
	 * @return Overlay
	 */
	public ArrayCircleOverlay getCircleOverlay() {
		return circleOverlay;
	}

	/**
	 * Function drawStart adds start to overlay
	 * @return Overlay with start point
	 */
	public ArrayWayOverlay drawStart() {
		GeoPoint point1 = new GeoPoint(start.getLatitude() + 0.00050,
				start.getLongitude());
		GeoPoint point2 = new GeoPoint(start.getLatitude() - 0.00025,
				start.getLongitude() - 0.00075);
		GeoPoint point3 = new GeoPoint(start.getLatitude() - 0.00025,
				start.getLongitude() + 0.00075);
		GeoPoint point4 = new GeoPoint(start.getLatitude() + 0.00050,
				start.getLongitude());

		OverlayWay wayStart = new OverlayWay(new GeoPoint[][] { { point1,
				point2, point3, point4 } }, null, null);
		wayOverlay.addWay(wayStart);
		helpWay[31] = wayStart;

		return wayOverlay;
	}

	/**
	 * Function removeStart deletes start from overlay
	 * @return Overlay without start point
	 */
	public ArrayWayOverlay removeStart() {
		wayOverlay.removeWay(helpWay[31]);
		return wayOverlay;
	}

	/**
	 * Function drawFinish adds finish to overlay
	 * @return Overlay with finish point
	 */
	public ArrayCircleOverlay drawFinish() {
		OverlayCircle circle1 = new OverlayCircle(finish, 70, null);
		OverlayCircle circle2 = new OverlayCircle(finish, 50, null);
		circleOverlay.addCircle(circle1);
		circleOverlay.addCircle(circle2);

		helpCircle[31] = circle1;
		helpCircle[32] = circle2;

		return circleOverlay;
	}

	/**
	 * Function removeFinish deletes finish from overlay
	 * @return Overlay without finish point
	 */
	public ArrayCircleOverlay removeFinish() {
		circleOverlay.removeCircle(helpCircle[31]);
		circleOverlay.removeCircle(helpCircle[32]);
		return circleOverlay;
	}
	
	/**
	 * Function drawControls adds all control points to the overlay
	 * @return Overlay with all controls
	 */
	public ArrayCircleOverlay drawControls() {
		OverlayCircle circle = null;
		for (int j = 0; j < countControl; j++) {
			circle = new OverlayCircle(points[j], 60, null);
			circleOverlay.addCircle(circle);
		}
		return circleOverlay;
	}

	/**
	 * Function drawControl adds control to overlay
	 * @param j Index of control
	 * @return Overlay with point
	 */
	public ArrayCircleOverlay drawControl(int j) {
		OverlayCircle circle = new OverlayCircle(points[j], 60, null);
		circleOverlay.addCircle(circle);
		helpCircle[getCountControls()] = circle;
		return circleOverlay;
	}

	/**
	 * Function removeControl deletes control from overlay
	 * @param j Index of control
	 * @return Overlay without chosen control
	 */
	public ArrayCircleOverlay removeControl(int j) {
		circleOverlay.removeCircle(helpCircle[j]);
		return circleOverlay;
	}

	/**
	 * Function drawWayBetweenControls draws every way between controls.
	 * @return Overlay with ways between controls
	 */
	public ArrayWayOverlay drawWayBetweenControls() {
		// Start to 1. control
		OverlayWay way = new OverlayWay(new GeoPoint[][] { { start, points[0] } });
		wayOverlay.addWay(way);
		// Between controls
		for (int j = 0; j < countControl - 1; j++) {
			way = new OverlayWay(new GeoPoint[][] { { points[j], points[j + 1] } });
			wayOverlay.addWay(way);
		}
		// Between last control and finish
		way = new OverlayWay(new GeoPoint[][] { { points[countControl - 1], finish } });
		wayOverlay.addWay(way);

		return wayOverlay;
	}
	
	/**
	 * Function drawWayBetween2Points add way between 2 points to overlay
	 * @param from One point as GeoPoint
	 * @param to Seconds point as GeoPoint
	 * @return Overlay with way
	 */
	public ArrayWayOverlay drawWayBetween2Points(GeoPoint from, GeoPoint to) {
		OverlayWay way = new OverlayWay(new GeoPoint[][] { { from, to } });
		wayOverlay.addWay(way);
		helpWay[getCountControls()] = way;
		return wayOverlay;
	}

	/**
	 * Function drawWayBetween2Points add way between 2 points to overlay
	 * @param i Index of one point
	 * @param point Point as GeoPoint
	 * @return Overlay with way
	 */
	public ArrayWayOverlay drawWayBetween2Points(int i, GeoPoint point) {
		OverlayWay way = new OverlayWay(new GeoPoint[][] { { point, points[i] } });
		wayOverlay.addWay(way);
		helpWay[getCountControls()] = way;
		return wayOverlay;
	}

	/**
	 * Function drawWayBetween2Points add way between 2 points to overlay
	 * @param i Index of one point
	 * @param j Index of seconds point
	 * @return Overlay with way
	 */
	public ArrayWayOverlay drawWayBetween2Points(int i, int j) {
		OverlayWay way = new OverlayWay(new GeoPoint[][] { { points[j], points[i] } });
		wayOverlay.addWay(way);
		helpWay[getCountControls()] = way;
		return wayOverlay;
	}

	/**
	 * Function deleteWayBetween2Points remove way between 2 points
	 * @return Overlay without the way
	 */
	public ArrayWayOverlay deleteWayBetween2Points() {
		wayOverlay.removeWay(helpWay[getCountControls()]);
		return wayOverlay;
	}
}