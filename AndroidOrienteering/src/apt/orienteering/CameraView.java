package apt.orienteering;

import java.io.IOException;
import android.content.Context;
import android.hardware.Camera;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class CameraView extends SurfaceView implements SurfaceHolder.Callback{
	
	private SurfaceHolder mHolder;
	private Camera mCamera;
	
	/** Constructors that will allow the View to be instantiated either 
	 * in code or through inflation from a resource layout.
	 */
	public CameraView(Context context){
		super(context);
		initCameraView();
	}
	
	public CameraView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initCameraView();
	}

	public CameraView(Context context, AttributeSet attrs, int defaultStyle) {
		super(context, attrs, defaultStyle);
		initCameraView();
	}
	
	/** Method initCameraView - set everything important for use of camera view */
	protected void initCameraView(){		
		mHolder = getHolder();
		mHolder.addCallback(this);
		mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
	}
	
	/** Open camera instance for using */
	public void openCamera(){
		try{
			mCamera = Camera.open();
		} catch (Exception e){
			// Error in opening camera
		}			
		mCamera.setDisplayOrientation(90);
	}
	
	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		try {
			mCamera.setPreviewDisplay(holder);
			mCamera.startPreview();
		} catch (IOException e) {
			Log.i("Error setting camera", e.getMessage());
		}		
	}
	
	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {		
		if (mHolder.getSurface() == null){
			return;
		}
		
		try {
			mCamera.stopPreview();
		} catch (Exception e){
			// Error in stop preview
		}
		
		Camera.Parameters parameters = mCamera.getParameters();
		parameters.setPreviewSize(w, h);
		
		try {
			mCamera.setPreviewDisplay(mHolder);
			mCamera.startPreview();
		} catch (Exception e){
			Log.i("Error starting camera", e.getMessage());
		}		
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		
	}

	/** Release camera instance for other applications after to use */
	public void releaseCamera(){
        if (mCamera != null){
            mCamera.release();	// Release the camera for other applications
            mCamera = null;
        }
    }
	
	/** Get angle of scanning by usage camera device */
	public float getVerticalViewAngle(){
		Camera.Parameters param = mCamera.getParameters();
		return (param.getVerticalViewAngle());
	}
}