package apt.orienteering;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class DoneTracks extends ListActivity {
	// Helpful variable for work with database
	Cursor model = null;
	TrackAdapter adapter = null;
	TrackHelper helper = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Set content view
		setContentView(R.layout.done_track);

		helper = new TrackHelper(this);
		initList();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		helper.close();
	}

	/** Function for onListItemClick - selection of track */
	@Override
	public void onListItemClick(ListView list, View view, int position, long id) {
		Intent i = new Intent(DoneTracks.this, TrackDetails.class);
		i.putExtra(ChooseTrack.ID_EXTRA, String.valueOf(helper.getId(model)));
		startActivity(i);
	}

	/** The function for initialization list of tracks */
	private void initList() {
		if (model != null) {
			stopManagingCursor(model);
			model.close();
		}

		model = helper.getDoneTracks("name");
		startManagingCursor(model);
		adapter = new TrackAdapter(model);
		setListAdapter(adapter);
	}

	/** The class TrackAdapter */
	class TrackAdapter extends CursorAdapter {
		TrackAdapter(Cursor c) {
			super(DoneTracks.this, c);
		}

		@Override
		public void bindView(View row, Context ctxt, Cursor c) {
			TrackHolder holder = (TrackHolder)row.getTag();
			holder.populateFrom(c, helper);
		}

		@Override
		public View newView(Context ctxt, Cursor c, ViewGroup parent) {
			LayoutInflater inflater = getLayoutInflater();
			View row = inflater.inflate(R.layout.item_done_track, parent, false);
			TrackHolder holder = new TrackHolder(row);
			row.setTag(holder);
			return (row);
		}
	}

	/** Static class TrackHolder for setting of text views in row */
	static class TrackHolder {
		private TextView name = null;
		private TextView author = null;
		private TextView control = null;
		private TextView length = null;
		private TextView winner = null;
		private TextView winnerTime = null;

		TrackHolder(View row) {
			name = (TextView) row.findViewById(R.id.d_track);
			author = (TextView) row.findViewById(R.id.d_author);
			control = (TextView) row.findViewById(R.id.d_control);
			length = (TextView) row.findViewById(R.id.d_length);
			winner = (TextView) row.findViewById(R.id.d_winner);
			winnerTime = (TextView) row.findViewById(R.id.d_winnertime);
		}

		// Set text to text views
		void populateFrom(Cursor c, TrackHelper helper) {
			name.setText(helper.getName(c));
			author.setText(helper.getAuthor(c));
			control.setText(String.valueOf(helper.getCountControl(c)) + " controls");
			length.setText(String.valueOf(helper.getLength(c)) + "m");
			winner.setText(helper.getWinner(c));
			winnerTime.setText(helper.getWinnerTime(c));
		}
	}
}