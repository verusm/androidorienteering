package apt.orienteering;

import java.io.File;
import org.mapsforge.android.maps.MapActivity;
import org.mapsforge.android.maps.MapView;
import org.mapsforge.android.maps.mapgenerator.MapGenerator;
import org.mapsforge.android.maps.mapgenerator.tiledownloader.MapnikTileDownloader;
import org.mapsforge.android.maps.overlay.ArrayWayOverlay;
import org.mapsforge.android.maps.overlay.OverlayWay;
import org.mapsforge.core.GeoPoint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

public class MapTabView extends MapActivity {
	// Global variables
	MapView map = null;
	Track track = new Track();
	TrackHelper helper = null;

	private int trackId = -1;
	private int raceId = -1;

	ArrayWayOverlay wayOverlay = null;
	
	File mapFile = null;
	MapGenerator mapG = null;
	private int flagMap = 0;
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		
		helper = new TrackHelper(this);
		
		// Information from TrackDetails
		trackId = getIntent().getIntExtra("trackId", -1);
		raceId = getIntent().getIntExtra("raceId", -1);
		
		if (trackId != -1) {
			// Get information from database about track
			getFromDatabase();
		}
		
		// Set content view
		setContentView(R.layout.map_tab_view);
		map = (MapView)findViewById(R.id.mapdonetrack);
		// Set map file and map generator
		mapFile = new File(track.getPath());
		mapG = map.getMapGenerator();
		
		// Set possible type of maps
		if (mapFile.exists()){
			// Off-line maps
			map.setMapGenerator(mapG);
			map.setMapFile(mapFile);
			flagMap = 0;
		} else if (Game.checkInternetConnection((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))){
			// Online Mapnik maps
			MapnikTileDownloader mapDown = new MapnikTileDownloader();
			map.setMapGenerator(mapDown);
			flagMap = 0;
		} else {
			// No saved file and no Internet connection
			Toast.makeText(this, "No saved file and no Internet connection - no possibility to display any map",
						Toast.LENGTH_LONG).show();
		}
		
		// Setting characters of mapView
		map.setClickable(true);
		map.getController().setCenter(track.getStart());
		map.setBuiltInZoomControls(true);
		map.getController().setZoom(14);
		map.setClickable(true);		
		map.getMapZoomControls().setZoomLevelMax((byte) 17);
		map.getMapZoomControls().setZoomLevelMin((byte) 12);
		
		// Draw track on the map
		map.getOverlays().add(track.drawStart());
		map.getOverlays().add(track.drawFinish());
		map.getOverlays().add(track.drawControls());
		// Paint for GPS track
		Paint wayPaintOutline = new Paint(Paint.ANTI_ALIAS_FLAG);
		wayPaintOutline.setStyle(Paint.Style.STROKE);
		wayPaintOutline.setColor(Color.RED);
		wayPaintOutline.setAlpha(128);
		wayPaintOutline.setStrokeWidth(3);
		wayPaintOutline.setStrokeJoin(Paint.Join.ROUND);

		wayOverlay = new ArrayWayOverlay(null, wayPaintOutline);
		// Draw GPS track on map
		drawGPStrack();
	}
	
	@Override
	public void onDestroy(){
		super.onDestroy();
		helper.close();
	}
	
	/** Method for drawing GPS track from race */
	private void drawGPStrack(){
		// Get information about race - GPS logs
		Cursor c = helper.getGPSlogs(String.valueOf(raceId));	
		c.moveToFirst();
		
		GeoPoint point1 = null;
		GeoPoint point2 = null;
		OverlayWay way = null;

		// Between GPS logs		
		for (int j = 0; j <= c.getCount()-2; j++) {
			point1 = new GeoPoint(helper.getLat(c), helper.getLon(c));
			c.moveToNext();
			point2 = new GeoPoint(helper.getLat(c), helper.getLon(c));
			way = new OverlayWay(new GeoPoint[][] {{ point1, point2}});
			// Add way to overlay
			wayOverlay.addWay(way);			
		}
		c.close();
		// Add overlay with GPS track on the map
		map.getOverlays().add(wayOverlay);		
	}
	
	/** Function for getting needful information about track from database */
	private void getFromDatabase() {
		// Cursor c = helper.getStart(id);
		Cursor c = helper.getInfoForGame(String.valueOf(trackId));
		c.moveToFirst();
		// Set information about track from database
		track.setId(helper.getId(c));
		track.setPath(helper.getState(c));
		track.setStart(helper.getStartLat(c), helper.getStartLon(c));
		track.setFinish(helper.getFinishLat(c), helper.getFinishLon(c));

		// Cursor for controls
		Cursor cc = helper.getControls(String.valueOf(track.getId()));
		// Count of controls (rows in cursor)
		track.setCountControls(cc.getCount());
		cc.moveToFirst();
		// Set controls
		for (int j = 0; j < cc.getCount(); j++) {
			track.setPoints(j, helper.getControlLat(cc),
					helper.getControlLon(cc));
			cc.moveToNext();
		}

		c.close();
		cc.close();
	}
	
	/** Set type of map */
	private void setMap(int item){
		// Implicit off-line maps
		if (item == 0){
			if (mapFile.exists()){
				if (flagMap == 1){
					map.setMapGenerator(mapG);
					map.setMapFile(mapFile);
					flagMap = 0;
				}
			} else{
				Toast.makeText(this, "No saved map file, use online maps", Toast.LENGTH_SHORT).show();
			}
		} else if (item == 1){
			if (Game.checkInternetConnection((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))){
				if(flagMap == 0){
					MapnikTileDownloader mapDown = new MapnikTileDownloader();
					map.setMapGenerator(mapDown);
					flagMap = 1;
				}
			} else { // No Internet connection
				Toast.makeText(this, "No Internet connection", Toast.LENGTH_SHORT).show();
			}
		}
	}
	
	/** Dialog for selection type of map */
	private void showDialogMaps(){
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Select type of map");
		builder.setSingleChoiceItems(R.array.maps, flagMap, new DialogInterface.OnClickListener() {
		    public void onClick(DialogInterface dialog, int item) {
		    	dialog.cancel();
		        setMap(item);
		    }
		})
		.show();
	}
	
	/** OPTIONS MENU */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		new MenuInflater(this).inflate(R.menu.option_track_details, menu);
		menu.removeItem(R.id.td_back);
		return (super.onCreateOptionsMenu(menu));
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.td_settings) {
			showDialogMaps();		
			return (true);
		}
		return (super.onOptionsItemSelected(item));
	}
}