package apt.orienteering;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

public class SavePlay extends Activity {
	// ID of chosen track
	String id = null;
	
	// Helpful variable for usage of database
	TrackHelper helper = null;
	Track track;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// No title bar
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		setContentView(R.layout.save_play);

		// Create buttons and set a onClickListener to them
		Button play = (Button)findViewById(R.id.b_play);
		play.setOnClickListener(onPlay);
		
		Button save = (Button)findViewById(R.id.b_save);
		save.setOnClickListener(onSave);
		
		// Information from last activity
		id = getIntent().getStringExtra(ChooseTrack.ID_EXTRA);
		
		helper = new TrackHelper(this);
		track = new Track();
	}
	
	@Override
	public void onDestroy(){
		super.onDestroy();
		helper.close();
	}
	
	/** OnClick method for button "Play" */
	private View.OnClickListener onPlay = new View.OnClickListener() {
		public void onClick(View v) {			
			if (checkMap()){			
				Intent i = new Intent(SavePlay.this, Game.class);
				i.putExtra(ChooseTrack.ID_EXTRA, id);
				// Start new Activity
				startActivity(i);	
			} else {
				Toast.makeText(SavePlay.this, "No saved map and no Internet connection!", Toast.LENGTH_LONG)
					.show();
			}
		}
	};
	
	/** Function for check accessible of some type of maps */
	private boolean checkMap(){		
		Cursor c = helper.getInfoForGame(id);
		c.moveToFirst();
		
		File mapFile = new File(helper.getState(c));		
		c.close();
		
		if (mapFile.exists() || checkInternetConnection()){
			return true;
		} else {
			Log.i("check map", "false");
			return false;
		}
	}
	
	/** OnClick method for button "Save track to XML" */
	private View.OnClickListener onSave = new View.OnClickListener() {
		public void onClick(View v) {
			// Create file and save track to it
			if (id != null){
				getTrackFromDatabase();
				saveTrackToXML();
			}
			
			Intent i = new Intent(SavePlay.this, AndroidOrienteering.class);
			i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			SavePlay.this.finish();
			// Back to the main menu
			startActivity(i);
		}
	};
	
	/** Function for download data about track from database */
	private void getTrackFromDatabase() {
		Cursor c = helper.getInfoForGame(id);
		c.moveToFirst();

		track.setId(helper.getId(c));
		track.setPath(helper.getState(c));
		track.setStart(helper.getStartLat(c), helper.getStartLon(c));
		track.setFinish(helper.getFinishLat(c), helper.getFinishLon(c));
		
		c.close();
		c = helper.getInfoTracks(track.getId());
		c.moveToFirst();
		track.setName(helper.getName(c));
		track.setAuthor(helper.getAuthor(c));
		track.setLength(helper.getLength(c));
		
		c.close();
		// Cursor for controls
		Cursor cc = helper.getControls(String.valueOf(track.getId()));

		// Count of controls (rows in cursor)
		track.setCountControls(cc.getCount());
		cc.moveToFirst();

		for (int j = 0; j < cc.getCount(); j++) {
			track.setPoints(j, helper.getControlLat(cc),
					helper.getControlLon(cc));
			cc.moveToNext();
		}
		cc.close();
	}
	
	/** Function for creation of XML file with saved track */
	private void saveTrackToXML(){		
		// Check and create Tracks folder
		AndroidOrienteering.checkAndCreateDirectory("/AndroidOrienteering/Tracks/");
		String trackName = track.getName();
		
		File trackFile = new File(Environment.getExternalStorageDirectory() + "/AndroidOrienteering/Tracks/" + trackName + ".xml");
		
		if (trackFile.exists()){
			// File with track already exists
			Toast.makeText(SavePlay.this, "File with track already exists!", Toast.LENGTH_LONG)
				.show();
		} else { // Create file with track
			try {
				FileOutputStream out = new FileOutputStream(new File(Environment.getExternalStorageDirectory()
						+ "/AndroidOrienteering/Tracks/", trackName + ".xml"));
				OutputStreamWriter osw = new OutputStreamWriter(out);
				String fileContent = "<name name=\"" + track.getName() + "\"></name>\n<author name=\"" + track.getAuthor() + "\"></author>\n" +
						"<controls count=\"" + track.getCountControls() + "\"></controls>\n<length length=\"" + track.getLength() + "\"></length>\n" +
						"<start lat=\"" + track.getStart().getLatitude() + "\" lon=\"" + track.getStart().getLongitude() + "\"></start>\n" + 
						"<finish lat=\"" + track.getFinish().getLatitude() + "\" lon=\"" + track.getFinish().getLongitude() + "\"></finish>\n" +
						"<state path=\"" + track.getPath() + "\"></state>\n";
				
				fileContent = fileContent + "<checkpoints>\n";
				for(int i = 0; i < track.getCountControls(); i++){
					fileContent = fileContent + "<point lat=\"" + track.getPoint(i).getLatitude() + "\" lon=\"" + track.getPoint(i).getLongitude() + "\"></point>\n";
				}
				fileContent = fileContent + "</checkpoints>";

				try {
					osw.write(fileContent);
				} catch (IOException e) {
					e.printStackTrace();
				}
				try {
					osw.flush();
				} catch (IOException e) {
					e.printStackTrace();
				}
				try {
					osw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		
			Toast.makeText(SavePlay.this, "The track is saved in the file " + trackName + " in folder /AndroidOrienteering/Tracks/", Toast.LENGTH_LONG)
				.show();
		}
	}
	
	/** Method for checking the Internet connection */
	private boolean checkInternetConnection() {
		ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		// Check the Internet connection
		if (conMgr.getActiveNetworkInfo() != null
				&& conMgr.getActiveNetworkInfo().isAvailable()
				&& conMgr.getActiveNetworkInfo().isConnected()) {
			return true;
		} else {
			return false;
		}
	}

}