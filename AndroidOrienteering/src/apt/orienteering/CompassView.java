package apt.orienteering;

import android.view.View;
import android.content.Context;
import android.util.AttributeSet;
import android.graphics.Paint;
import android.content.res.Resources;
import android.graphics.Canvas;

public class CompassView extends View {
	/** Constructors that will allow the View to be instantiated either
	 * in code or through inflation from a resource layout.
	 */
	public CompassView(Context context) {
		super(context);
		initCompassView();
	}

	public CompassView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initCompassView();
	}

	public CompassView(Context context, AttributeSet attrs, int defaultStyle) {
		super(context, attrs, defaultStyle);
		initCompassView();
	}

	/** Objects for drawing compass */
	private Paint arrow;
	private Paint greylinie;
	private Paint bluelinie;
	private Paint textNorth;
	private Paint text;
	private Paint circle;
	private String north;
	private String east;
	private String south;
	private String west;
	
	/** The variable for azimuth */
	private float bearing;

	/** Initialize the control and call it from each constructor. */
	protected void initCompassView() {
		// Set this view focusable
		setFocusable(true);

		Resources r = this.getResources();

		// Define each Paint
		arrow = new Paint(Paint.ANTI_ALIAS_FLAG);
		arrow.setColor(r.getColor(R.color.red));
		arrow.setStrokeWidth(5);

		greylinie = new Paint(Paint.ANTI_ALIAS_FLAG);
		greylinie.setColor(r.getColor(R.color.grey));
		greylinie.setStrokeWidth(2);

		bluelinie = new Paint(Paint.ANTI_ALIAS_FLAG);
		bluelinie.setColor(r.getColor(R.color.blue));
		bluelinie.setStrokeWidth(5);

		textNorth = new Paint(Paint.ANTI_ALIAS_FLAG);
		textNorth.setColor(r.getColor(R.color.red));
		textNorth.setTextSize(35);

		text = new Paint(Paint.ANTI_ALIAS_FLAG);
		text.setColor(r.getColor(R.color.blue));
		text.setTextSize(35);

		circle = new Paint(Paint.ANTI_ALIAS_FLAG);
		circle.setColor(r.getColor(R.color.black));
		circle.setStrokeWidth(5);
		circle.setStyle(Paint.Style.STROKE);	// only outline of circle

		// Set strings
		north = r.getString(R.string.cardinal_north);
		east = r.getString(R.string.cardinal_east);
		south = r.getString(R.string.cardinal_south);
		west = r.getString(R.string.cardinal_west);
	}

	/** Measure size for this view and it's children 
	 * @param widthMeasureSpec Horizontal space requirements as imposed by the parent
	 * @param heightMeasureSpec Vertical space requirements as imposed by the parent
	 * */
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int measuredWidth = measure(widthMeasureSpec);
		int measuredHeight = measure(heightMeasureSpec);
		
		// Set measured dimension
		setMeasuredDimension(measuredWidth, measuredHeight);
	}

	/** Size from parent object */
	private int measure(int measSpec) {
		int specMode = MeasureSpec.getMode(measSpec);
		int specSize = MeasureSpec.getSize(measSpec);

		if (specMode == MeasureSpec.UNSPECIFIED) {
			// The size is not known from parent - set 200
			return 200;
		} else {
			// The size is specific from parent - return it
			return specSize;
		}
	}

	/** Set azimuth
	 * @param b Azimuth
	 */
	public void setBearing(float b) {
		bearing = b;
	}

	/** Draw compass by using prepared objects */
	@Override
	protected void onDraw(Canvas canvas) {
		// Helping variables
		int px = getMeasuredWidth() / 2;
		int py = getMeasuredHeight() / 2;
		int radius = Math.min(px, py);
		// Set radius smaller than maximal possible
		radius = radius - 40;

		int mark = getMeasuredWidth() / 6;

		// Draw grey lines on the background of compass
		canvas.drawLine(0, 0, 0, getMeasuredHeight(), greylinie);
		canvas.drawLine(mark, 0, mark, getMeasuredHeight(), greylinie);
		canvas.drawLine(mark * 2, 0, mark * 2, getMeasuredHeight(), greylinie);
		canvas.drawLine(mark * 3, 0, mark * 3, getMeasuredHeight(), greylinie);
		canvas.drawLine(mark * 4, 0, mark * 4, getMeasuredHeight(), greylinie);
		canvas.drawLine(mark * 5, 0, mark * 5, getMeasuredHeight(), greylinie);
		canvas.drawLine(getMeasuredWidth(), 0, getMeasuredWidth(), getMeasuredHeight(), greylinie);

		// Draw circle of compass
		canvas.drawCircle(px, py, radius, circle);

		// Set rotation of compass
		canvas.save();
		canvas.rotate(-bearing, px, py);
	
		// Draw arrow point to north
		canvas.drawLine(px, py - radius, px, py, arrow);
		canvas.drawLine(px, py - radius, px - 20, py - radius + 20, arrow);
		canvas.drawLine(px, py - radius, px + 20, py - radius + 20, arrow);

		// Draw line to south
		canvas.drawLine(px, py + radius, px, py, bluelinie);

		int textWidth = (int) text.measureText("W");
		
		// Draw letters of courses
		canvas.drawText(north, px - textWidth / 2, py - radius - 10, textNorth);
		canvas.drawText(east, px + radius + 10, py + textWidth / 2, text);
		canvas.drawText(south, px - textWidth / 2, py + radius + 30, text);
		canvas.drawText(west, px - radius - 40, py + textWidth / 2 + 30, text);
		canvas.restore();	
	}
}