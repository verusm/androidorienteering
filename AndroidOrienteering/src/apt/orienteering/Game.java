package apt.orienteering;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.mapsforge.android.maps.MapActivity;
import org.mapsforge.android.maps.MapView;
import org.mapsforge.android.maps.mapgenerator.MapGenerator;
import org.mapsforge.android.maps.mapgenerator.tiledownloader.MapnikTileDownloader;
import org.mapsforge.android.maps.overlay.ArrayCircleOverlay;
import org.mapsforge.android.maps.overlay.OverlayCircle;
import org.mapsforge.core.GeoPoint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Paint;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import apt.orienteering.ChooseTrack.TrackAdapter;

public class Game extends MapActivity{
	// Constant
	static final int COUNT_VALUES = 10;
	// Global variables
	Cursor model = null;
	TrackAdapter adapter = null;
	TrackHelper helper = null;
	
	MapView map = null;
	CompassView compass = null;
	CameraView camera = null;
	LineView line = null;
	RelativeLayout info = null;
	
	TextView textControl = null;
	TextView titleText = null;
	EditText player = null;
	
	Button foundControl;
	Button startButton;
	// Mode of game - map, compass, camera, info, settings
	private String mode = "map";
	// Selected track from list
	private String id = null;
	private int raceId = -1;
	
	private int searchingControl = 0;
	private boolean flagStart = false;	// Game is in progress
	private boolean flagFinish = false;	// Game is in finish point
	private boolean flagSavedRace = false; // Race is saved

	OverlayCircle circle1 = null;
	OverlayCircle circle2 = null;
	ArrayCircleOverlay currentLocation = null;

	Track track = new Track();
	// Shared preferences
	SharedPreferences prefs = null;
	SharedPreferences.Editor editor = null;

	LocationManager locMgr = null;
	// Date format for saving to database
	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSZ");
	private String currentTime = null;
	private String stringTime = null;
	
	private int tolerance = 0;
	float distanceToControl = 0;

	// Sensors helping values
	SensorManager sensorManager;
	private Sensor sensorAccelerometer;
	private Sensor sensorMagneticField;
	private float[] vAccelerometer = new float[3];
	private float[] vMagneticField = new float[3];  
	private float[] matrixR = new float[9];
	private float[] matrixI = new float[9];
	private float[] matrixValues = new float[9];
	private float[] arrayx = new float[COUNT_VALUES];

	File mapFile = null;
	MapGenerator mapG = null;
	// flag which map is set
	private boolean flag = false;

	// flags for set providers
	private boolean GPS = false;
	private boolean network = false;	
	private boolean registerSensors = false;

	private Location lastFix = null;
	
	// Variables for information mode
	TextView tv_searchingControl;
	TextView tv_location;
	TextView tv_accuracyGPS;
	TextView tv_accuracyNetwork;
	TextView tv_tolerance;
	TextView tv_currentLocation;
	TextView tv_map;
	
	// Screen can/cann't sleep
	PowerManager pm = null;
	PowerManager.WakeLock wl = null;	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);		
		// No title bar
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		helper = new TrackHelper(this);
		// ID from last activity - chosen track
		id = getIntent().getStringExtra(ChooseTrack.ID_EXTRA);
		if (id != null) {
			// Get information about track from database
			getFromDatabase();
		}
		
		// Sleeping screen
		pm = (PowerManager)getSystemService(Context.POWER_SERVICE);
		wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "Tag");
		
		// SharedPreferences
		prefs=PreferenceManager.getDefaultSharedPreferences(this);
		prefs.registerOnSharedPreferenceChangeListener(prefListener);		
		editor = prefs.edit();
	
		// Set content view
		setContentView(R.layout.game);	
		// Set components for view		
		map = (MapView)findViewById(R.id.mapview);
		compass = (CompassView)findViewById(R.id.compassview);
		camera = (CameraView)findViewById(R.id.camera);
		line = (LineView)findViewById(R.id.line);
		info = (RelativeLayout)findViewById(R.id.info_layout);
		foundControl = (Button)findViewById(R.id.b_foundControl);
		foundControl.setOnClickListener(onFoundControl);
		startButton = (Button) findViewById(R.id.b_start);
		// Variables for information mode
		tv_searchingControl = (TextView)findViewById(R.id.searching_control);
		tv_location = (TextView)findViewById(R.id.clocation);
		tv_accuracyGPS = (TextView)findViewById(R.id.gps_accuracy);
		tv_accuracyNetwork = (TextView)findViewById(R.id.network_accuracy);
		tv_tolerance = (TextView)findViewById(R.id.tolerance);		
		tv_currentLocation = (TextView)findViewById(R.id.current_location);
		tv_map = (TextView)findViewById(R.id.map_type);
	
		// Set sensors values
		sensorManager = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
	    sensorAccelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
	    sensorMagneticField = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
	    
	    // Set map file
	    mapFile = new File(track.getPath());
	    mapG = map.getMapGenerator();
		Log.i("map generator", String.valueOf(mapG));
	    
	    // Set map
	 	setInitialMap(prefs.getString("map_type", "Offline Maps"));
		 		
		// Set characters of map		
		map.getController().setCenter(track.getStart());
		map.setBuiltInZoomControls(true);
		map.getController().setZoom(14);		
		map.getMapZoomControls().setZoomLevelMax((byte) 17);
		map.getMapZoomControls().setZoomLevelMin((byte) 12);
		map.setClickable(true);

		map.getOverlays().add(track.drawStart());
		
		// Set attributes for circle - currentLocation
		Paint circlePaintOutline = new Paint(Paint.ANTI_ALIAS_FLAG);
		circlePaintOutline.setStyle(Paint.Style.STROKE);
		circlePaintOutline.setColor(Color.RED);
		circlePaintOutline.setAlpha(128);
		circlePaintOutline.setStrokeWidth(3);

		currentLocation = new ArrayCircleOverlay(null, circlePaintOutline);

		// Location manager
		locMgr = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
		setLocationProvider();

		tolerance = Integer.parseInt(prefs.getString("toleranc", "0"));			
		tv_tolerance.setText(String.valueOf(tolerance) + " meters");
		
		tv_accuracyGPS.setText("NO");
		tv_accuracyNetwork.setText("NO");
			
		if (prefs.getBoolean("current_location", false)){
			distanceToControl = Float.parseFloat(prefs.getString("cl_distance", "0"));
			tv_currentLocation.setText(String.valueOf(distanceToControl) + " meters in front of the control");				
		}
		
		Toast.makeText(Game.this, "Go to the START!", Toast.LENGTH_SHORT).show();
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		if (registerSensors){
			unregisterSensors();
		}
		camera.releaseCamera();
		locMgr.removeUpdates(onLocationChange);		
		mode = "map";	
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();

		if (flagSavedRace){
			helper.deleteRace(raceId); // Delete unsaved race from database
		}
		
		helper.close();
		locMgr.removeUpdates(onLocationChange);
		camera.releaseCamera();
		if (registerSensors){
			unregisterSensors();
		}
	}
	
	@Override
	public void onPause(){
		super.onPause();
		if (registerSensors){
			unregisterSensors();
		}
		locMgr.removeUpdates(onLocationChange);
		camera.releaseCamera();
		mode = "map";
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		if (!flagFinish){
			setLocationProvider();
		}
	}
	
	/** OnClick method for button with found control */
	private View.OnClickListener onFoundControl = new View.OnClickListener() {
		public void onClick(View v) {
			/* if (flagFinish){
				gameOver();
			} else {
			*/
			foundControl.setVisibility(View.GONE);
			
		}
	};
	
	/** Function for converting type String to Date with df format */
	private Date convertStringToDate(String dateString) {
		Date date = null;	
		try {
			date = df.parse(dateString);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
		return date;
	}
	
	/** After find finish point */
	public void gameOver() {
		// Set content view for saving done track
		setContentView(R.layout.done_race);
		// Access to database for count time of race 
		Cursor c = helper.getGPSlogs(String.valueOf(raceId));
		c.moveToFirst();
		Date startDate = convertStringToDate(helper.getTimestamp(c));
		c.moveToLast();
		Date finishDate = convertStringToDate(helper.getTimestamp(c));
		Date totalTime = new Date(finishDate.getTime() - startDate.getTime());
		
		long miliseconds = totalTime.getTime();		
		stringTime = milisecondsToDateString(miliseconds);

		// Display total time
		TextView time = (TextView) findViewById(R.id.total_time);
		time.setText(stringTime);
		// Read player's name from EditText
		player = (EditText) findViewById(R.id.runner);

		Button save = (Button) findViewById(R.id.save_race);
		save.setOnClickListener(onSaveRace);
		c.close();
	}
	
	/** Function for converting long to string with time (date) */
	public static String milisecondsToDateString(long miliseconds){
		int minutes = 0;
		int hours = 0;
		int seconds = (int)miliseconds/1000;
		
		hours = seconds/3600;
		seconds = seconds - (hours*3600);
		minutes = seconds/60;
		seconds = seconds - (minutes*60);
		
		String hStr = null;
		String mStr = null;
		String sStr = null;
		
		if (hours < 10){
			hStr = "0" + String.valueOf(hours);
		} else {
			hStr = String.valueOf(hours);
		}
		
		if (minutes < 10){
			mStr = "0" + String.valueOf(minutes);
		} else {
			mStr = String.valueOf(minutes);
		}
		
		if (seconds < 10){
			sStr = "0" + String.valueOf(seconds);
		} else {
			sStr = String.valueOf(seconds);
		}		
		return (hStr + ":" + mStr + ":" + sStr);
	}

	/** OnClick on button Save - saving race */
	private View.OnClickListener onSaveRace = new View.OnClickListener() {
		public void onClick(View v) {
			if (player.getText().toString().equals("")){
				// No string in name of player
				showAlertDialog();
			} else { // Save race to database
				helper.saveDoneRace(raceId, stringTime, player.getText().toString(), track.getId());
				flagSavedRace = false;
				
				// Screen can sleep
				wl.release();

				// Go back to the start screen
				Intent i = new Intent(Game.this, AndroidOrienteering.class);
				Game.this.finish();
				i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(i);
			}
		}
	};
	
	/** The method for showing Alert Dialog - no name of the player */
	private void showAlertDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("No runner name")
				.setMessage("Please enter a name of runner!")
				.setPositiveButton("OK", null).show();
	}
	
	/** Listener for location */
	private LocationListener onLocationChange = new LocationListener() {
		@Override
		public void onLocationChanged(Location fix) {
			if (!flagFinish){
			
			// Save lastKnownFix
			lastFix = fix;			
			
			TextView textFix = (TextView)findViewById(R.id.textControl);
			Date curTime = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
			String strTime = sdf.format(curTime);
			// Display provider, fix and time
			textFix.setText(fix.getProvider() + " location in " + strTime + "\n" + "lat " + fix.getLatitude() + "\n" + "lon " + fix.getLongitude());
			
			if (fix.getProvider().equals("gps")){
				tv_accuracyGPS.setText(String.valueOf(fix.getAccuracy()) + " meters");
			} else if (fix.getProvider().equals("network")){
				tv_accuracyNetwork.setText(String.valueOf(fix.getAccuracy()) + " meters");
			}
			
			// Game running
			if (flagStart){
				currentTime = df.format(new Date());
			
				if (searchingControl == track.getCountControls()) { // Searching of finish point
					if (checkCurrentLocation(fix, track.getFinish(), distanceToControl)) {
						displayCurrentLocation(fix);
					}
					if (checkCurrentLocation(fix, track.getFinish())) { // Finish was found
						helper.insertGPSlog(raceId, fix.getLatitude(), fix.getLongitude(), currentTime, 1);	
						// Set button finish
						startButton.setText("FINISH");
						startButton.setVisibility(View.VISIBLE);
						// No more current location
						locMgr.removeUpdates(onLocationChange);
						// Set map mode
						mode = "map";
						changeView();
						flagFinish = true;						
					} else { // Nothing found
						// Only save GPSlog to database
						helper.insertGPSlog(raceId, fix.getLatitude(), fix.getLongitude(), currentTime, 0);
					}
				} else { // Searching of controls
					if (checkCurrentLocation(fix, track.getPoint(searchingControl), distanceToControl)){
						displayCurrentLocation(fix); // Display current location
					}
					if (checkCurrentLocation(fix, track.getPoint(searchingControl))) { // Searching control was found
						searchingControl++;
					
						foundControl.setText(searchingControl + ". control");
						foundControl.setVisibility(View.VISIBLE);
					
						if (searchingControl == track.getCountControls()){
							tv_searchingControl.setText("Finish point!");
						} else {
							tv_searchingControl.setText(String.valueOf(searchingControl + 1) + ". control");
						}

						helper.insertGPSlog(raceId, fix.getLatitude(), fix.getLongitude(), currentTime, 1);
					} else { // Nothing found
						// Only save GPSlog to database
						helper.insertGPSlog(raceId, fix.getLatitude(), fix.getLongitude(), currentTime, 0);
					}
				}
			} else { // Way to the start point
				if (checkCurrentLocation(fix, track.getStart())) { // On start point
					startButton.setVisibility(View.VISIBLE);
					locMgr.removeUpdates(onLocationChange);					

					map.setClickable(false);
					map.setBuiltInZoomControls(false);
					
					startButton.setOnClickListener(onButtonStart);
				} else {
					displayCurrentLocation(fix);
				}
			}
			}
		}
		
		/** Listener for button START */
		private View.OnClickListener onButtonStart = new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (flagFinish){
					gameOver();
				} else {
					// Don't sleep
					wl.acquire();
				
					startButton.setVisibility(View.GONE);
					// Draw track on map
					map.getOverlays().add(track.drawFinish());
					map.getOverlays().add(track.drawControls());
					map.getOverlays().add(track.drawWayBetweenControls());

					map.setClickable(true);
					map.setBuiltInZoomControls(true);

					// Current time
					currentTime = df.format(new Date());
				
					// Create race in database
					helper.createRace(track.getId(), currentTime);
					flagSavedRace = true;
					raceId = helper.getRaceId();
					// Save first log
					helper.insertGPSlog(raceId, lastFix.getLatitude(), lastFix.getLongitude(), currentTime, 0);
					// Set flag
					flagStart = true;
					// Set text in INFO - searching control
					tv_searchingControl.setText("1. control");
					// Start listener for current location
					setLocationProvider();
				}
			}
		};

		@Override
		public void onProviderDisabled(String provider) {
			Toast.makeText(Game.this, provider + " provider is not enabled!", Toast.LENGTH_SHORT).show();
		}

		@Override
		public void onProviderEnabled(String provider) {
			Toast.makeText(Game.this, provider + " provider enable!", Toast.LENGTH_SHORT).show();
		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {

		}
	};
	
	/** Check current location - is the location on the searching point or not? */
	public boolean checkCurrentLocation(Location loc, GeoPoint point) {
		Location loc2 = new Location("GPS");
		loc2.setLatitude(point.getLatitude());
		loc2.setLongitude(point.getLongitude());

		float distance = loc.distanceTo(loc2);
		
		if (distance <= tolerance) { // Current position is in tolerance
			removeCurrentLocation();
			return true;
		} else {
			return false;
		}
	}
	
	/** Check current location - is the location on the searching point or not? 
	 * Function for control of displaying current location in front of the control */
	public boolean checkCurrentLocation(Location loc, GeoPoint point, float tolerance) {
		Location loc2 = new Location("GPS");
		loc2.setLatitude(point.getLatitude());
		loc2.setLongitude(point.getLongitude());

		float distance = loc.distanceTo(loc2);
		
		if (distance <= tolerance) {
			// Current location is in distance for displaying => display current location on the map
			removeCurrentLocation();
			return true;
		} else {
			return false;
		}
	}
	
	/** Remove point, which display current location in the map */
	public void removeCurrentLocation(){
		if (circle1 != null){
			currentLocation.removeCircle(circle1);
		}
		if (circle2 != null){
			currentLocation.removeCircle(circle2);
		}
	}
	
	/** Add point with current location on the map */
	public void displayCurrentLocation(Location fix){
		GeoPoint point = new GeoPoint(fix.getLatitude(), fix.getLongitude());
		if (circle1 == null) {
			circle1 = new OverlayCircle(point, 5, null);
			currentLocation.addCircle(circle1);
			if (circle2 != null) {
				currentLocation.removeCircle(circle2);
			}
		} else {
			circle2 = new OverlayCircle(point, 5, null);
			currentLocation.removeCircle(circle1);
			currentLocation.addCircle(circle2);
			circle1 = null;
		}
		map.getOverlays().add(currentLocation);
	}
	
	/** Set information about track from database */
	private void getFromDatabase() {
		Cursor c = helper.getInfoForGame(id);
		c.moveToFirst();

		track.setId(helper.getId(c));
		track.setPath(helper.getState(c));
		track.setStart(helper.getStartLat(c), helper.getStartLon(c));
		track.setFinish(helper.getFinishLat(c), helper.getFinishLon(c));

		// Cursor for controls
		Cursor cc = helper.getControls(String.valueOf(track.getId()));
		// Count of controls (rows in cursor)
		track.setCountControls(cc.getCount());
		cc.moveToFirst();

		for (int j = 0; j < cc.getCount(); j++) {
			track.setPoints(j, helper.getControlLat(cc), helper.getControlLon(cc));
			cc.moveToNext();
		}

		c.close();
		cc.close();
	}
	
	// COMPASS and CAMERA
	/** Initialize array for values and array for lastValues - set to 0 */
	private void initArrayX(){
		for (int i = 0; i < COUNT_VALUES; i++){
			arrayx[i] = 0.0f;
		}
		flagX = 0;
		flagFirstRound = true;
		
	}
	
	private int flagX = 0;
	private boolean flagFirstRound = true;
	private boolean flagMin = false;
	private boolean flagMax = false;
	
	/** Filter for sensors */
	private float filter(float x){
		float resultx = 0.0f;
		
		// Pointer to array
		if (flagX == COUNT_VALUES){
			flagX = 0;
			flagFirstRound = false;
		}
		
		arrayx[flagX] = x;
		
		if (flagFirstRound){
			for (int i = 0; i < flagX; i++){
				resultx = resultx + arrayx[i];
			}
			resultx = resultx / (flagX+1);
		} else {
			for (int i = 0; i < COUNT_VALUES; i++){				
				for (int j = 0; j < COUNT_VALUES; j++){
					// If in the array are values near 180 and -180
					if (arrayx[j] > 170){
						flagMax = true;
					}
					if (arrayx[j] < (-170)){
						flagMin = true;
					}
				}				
				// If in the array are values near 180 and -180 - add 360
				if (flagMin && flagMax && arrayx[i] < (-170)){
					resultx = resultx + arrayx[i] + 360;
				} else {
					resultx = resultx + arrayx[i];
				}
			}
			// Make average
			resultx = resultx / COUNT_VALUES;
		}
		
		// Set flags
		flagX++;
		flagMax = false;
		flagMin = false;
		
		return resultx;
	}
	
	/** Sensor event listener */
	private final SensorEventListener sensorEventListener = new SensorEventListener() {
		public void onSensorChanged(SensorEvent event) {
			if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER){
				vAccelerometer = event.values;
			}
			if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD){
				vMagneticField = event.values; 
			}
			
			updateOrientation();
		}
		
		public void onAccuracyChanged(Sensor sensor, int accuracy) {
		};
	};
	
	float lastValue = 0;
	
	/** Update data from sensors */
	private void updateOrientation(){
		boolean success = SensorManager.getRotationMatrix(
			       matrixR,
			       matrixI,
			       vAccelerometer,
			       vMagneticField);

		if (success){
			SensorManager.getOrientation(matrixR, matrixValues);
			matrixValues[0] = (float) Math.toDegrees(matrixValues[0]);
			matrixValues[1] = (float) Math.toDegrees(matrixValues[1]);
			matrixValues[2] = (float) Math.toDegrees(matrixValues[2]);
			
			if (mode.equals("compass")){
				compass.setBearing((int)filter(matrixValues[0]));
				compass.invalidate();
			} else if (mode == "camera"){
				if (line != null){								
					Location sControl = new Location("GPS");
					  
					if (searchingControl > track.getCountControls()){ // Find finish
						sControl.setLatitude(track.getFinish().getLatitude());
						sControl.setLongitude(track.getFinish().getLongitude());
					} else { // Find control
						sControl.setLatitude(track.getPoint(searchingControl).getLatitude());
						sControl.setLongitude(track.getPoint(searchingControl).getLongitude());
					}

					line.setBearing(lastFix.bearingTo(sControl));
						
					line.setX(matrixValues[1]);
					
					float value = matrixValues[0];
					// Average from last 2 values
					value = (value + 4*lastValue)/5;					
					lastValue = value;
					
					line.setZ(value);				   
					line.setAngle(camera.getVerticalViewAngle());	
					line.invalidate();						
				}
			}
		}
	}
	
	/** Register sensors */
	private void registerSensors(){	
		if (mode == "camera"){
			sensorManager.registerListener(sensorEventListener, sensorAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
			sensorManager.registerListener(sensorEventListener, sensorMagneticField, SensorManager.SENSOR_DELAY_NORMAL);
		} else {
			sensorManager.registerListener(sensorEventListener, sensorAccelerometer, SensorManager.SENSOR_DELAY_FASTEST);
			sensorManager.registerListener(sensorEventListener, sensorMagneticField, SensorManager.SENSOR_DELAY_FASTEST);
		}
		registerSensors = true;
		initArrayX();
	}
	
	/** Unregister sensors */ 
	private void unregisterSensors(){
		sensorManager.unregisterListener(sensorEventListener, sensorAccelerometer);
		sensorManager.unregisterListener(sensorEventListener, sensorMagneticField);
		registerSensors = false;
	}
	
	/** Check if this device has a camera */
	private boolean checkCamera(Context context) {
	    if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
	        // This device has a camera
	        return true;
	    } else {
	        return false;
	    }
	}
	
	/** Check if the device has needful sensors */
	private boolean checkSensors(){
		boolean accelerometer = false;
		boolean magneticField = false;
		
		if (sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null){
			// This device has accelerometer
			accelerometer = true;
		}
		if (sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD) != null){
			// This device has magnetic array
			magneticField = true;
		}
		return (accelerometer && magneticField);
	}
	
	/** Change mode - map, compass, camera, info, settings */
	private void changeView(){		
		if (mode == "map"){
			// Release camera and unregister sensors
			if (registerSensors){
				unregisterSensors();
			}
			camera.releaseCamera();
			
			
			compass.setVisibility(View.GONE);
			compass.destroyDrawingCache();
			compass.invalidate();
			camera.setVisibility(View.GONE);
			camera.invalidate();
			line.setVisibility(View.GONE);
			line.destroyDrawingCache();
			line.invalidate();
			info.setVisibility(View.GONE);
			info.invalidate();
		} else if (mode == "compass"){			
			registerSensors();
			camera.releaseCamera();
			
			info.setVisibility(View.GONE);
			info.invalidate();
			camera.setVisibility(View.GONE);
			camera.invalidate();
			line.setVisibility(View.GONE);
			line.destroyDrawingCache();
			line.invalidate();
			compass.setVisibility(View.VISIBLE);
		} else if (mode == "camera"){			
			registerSensors();
			camera.openCamera();
			
			compass.setVisibility(View.GONE);
			compass.destroyDrawingCache();
			compass.invalidate();
			info.setVisibility(View.GONE);
			info.invalidate();
			camera.setVisibility(View.VISIBLE);
			line.setVisibility(View.VISIBLE);
		} else if (mode == "info"){			
			if (registerSensors){
				unregisterSensors();
			}
			camera.releaseCamera();
			
			compass.setVisibility(View.GONE);
			compass.destroyDrawingCache();
			compass.invalidate();
			camera.setVisibility(View.GONE);
			camera.invalidate();
			line.setVisibility(View.GONE);
			line.destroyDrawingCache();
			line.invalidate();
			info.setVisibility(View.VISIBLE);
		} else if (mode == "settings"){
			if (registerSensors){
				unregisterSensors();
			}
			camera.releaseCamera();
			
			camera.setVisibility(View.GONE);
			camera.invalidate();
			line.setVisibility(View.GONE);
			line.destroyDrawingCache();
			line.invalidate();
			compass.setVisibility(View.GONE);
			compass.destroyDrawingCache();
			compass.invalidate();
		}
	}
	
	/** Check the Internet connection */
	public static boolean checkInternetConnection(ConnectivityManager conMgr) {
		// Check the Internet connection
		if (conMgr.getActiveNetworkInfo() != null
				&& conMgr.getActiveNetworkInfo().isAvailable()
				&& conMgr.getActiveNetworkInfo().isConnected()) {
			return true;
		} else {
			return false;
		}
	}
	
	/** Set location provider which is set in preferences or which is enable */
	private void setLocationProvider(){
		String value = prefs.getString("location", "Network");
		
		if (value.equals("Network")){
			if(!checkInternetConnection((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))){
				Toast.makeText(this, "No Internet connection", Toast.LENGTH_SHORT).show();
				network = false;
				GPS = true;
			} else {
				network = true;
				GPS = false;
			}
		} else if (value.equals("GPS")){
			if (!locMgr.isProviderEnabled(LocationManager.GPS_PROVIDER)){
				Toast.makeText(this, "GPS disable - start GPS", Toast.LENGTH_SHORT).show();
			}
			GPS = true;
			network = false;
		} else if (value.equals("GPS and Network")){
			if(!checkInternetConnection((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))){
				Toast.makeText(this, "No Internet connection", Toast.LENGTH_SHORT).show();
				network = false;
			} else {
				GPS = true;
				network = true;
			}
		}
		
		locMgr.removeUpdates(onLocationChange);
		// Set location providers
		if (GPS && network){
			locMgr.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000*10, 2, onLocationChange);
			locMgr.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000*10, 2, onLocationChange);
			tv_location.setText("GPS and Network");
		} else if (GPS){
			locMgr.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000*10, 2, onLocationChange);
			tv_location.setText("GPS");
		} else if (network){
			locMgr.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000*10, 2, onLocationChange);
			tv_location.setText("Network");
		}		
	}
	
	/** Set type of map after start of activity */
	private void setInitialMap(String typeMap){
		if (mapFile.exists()){
			map.setMapFile(mapFile);
			mapG = map.getMapGenerator();
			Log.i("mapG", String.valueOf(mapG));
			flag = true;
			tv_map.setText("Offline Map");
			editor.putString("map_type", "Offline Maps");
			editor.commit();
		} else if (checkInternetConnection((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))){
			MapnikTileDownloader mapDown = new MapnikTileDownloader();
			map.setMapGenerator(mapDown);
			flag = false;
			tv_map.setText("Mapnik - online");
			editor.putString("map_type", "Mapnik - online");
			editor.commit();
		} else { // Only for sure - is not possible to be there
			Toast.makeText(Game.this, "No possible maps!", Toast.LENGTH_SHORT).show();
		}
	}
	
	/** Set type of map */
	private void setMap(){
		String value = prefs.getString("map_type", "Offline Maps");
		
		if (value.equals("Offline Maps")){
			if (mapFile.exists()){
				if (!flag){
					Log.i("map generator set offline", String.valueOf(mapG));
					map.setMapGenerator(mapG);
					map.setMapFile(mapFile);
					flag = true;
					tv_map.setText("Offline Maps");					
				}
			} else {
				Toast.makeText(this, "No saved map file, use online maps", Toast.LENGTH_SHORT).show();
			}
		} else if (value.equals("Mapnik - online")){
			if (checkInternetConnection((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))){
				if(flag){
					MapnikTileDownloader mapDown = new MapnikTileDownloader();
					map.setMapGenerator(mapDown);
					flag = false;					
					tv_map.setText("Mapnik - online");
				}
			} else {
				Toast.makeText(this, "No Internet connection", Toast.LENGTH_SHORT).show();
			}
		} else{
			Toast.makeText(this, "No possible maps!", Toast.LENGTH_SHORT).show();
		}
	}

	/** Method for keys */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) { // Key BACK
			mode = "map";
			changeView();
			showExitDialog();
		}
		return super.onKeyDown(keyCode, event);
	}
	
	/** The method for showing closing dialog */
	private void showExitDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("Do you really want to quit the track? The result of the track will be deleted!")
				.setCancelable(false)
				.setPositiveButton("YES", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// Go back to menu
						Intent i = new Intent(Game.this, AndroidOrienteering.class);
						Game.this.finish();
						i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(i);
					}
				})
				.setNegativeButton("NO", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				}).show();
	}
	
	/** OPTIONS MENU */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		new MenuInflater(this).inflate(R.menu.option_game, menu);
		// Mode camera is enable after only in game (after start)
		menu.findItem(R.id.camera).setEnabled(false); 
		return (super.onCreateOptionsMenu(menu));
	}
	
	/** The method for selection from options menu */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {		
		if (item.getItemId() == R.id.map) { // Option MAP - display map
			mode = "map";
			changeView();			
			return (true);
		} else if (item.getItemId() == R.id.compass) { // Option COMPASS - display compass
			if (mode.equals("compass")){				
				mode = "map";
				changeView();
			} else if (checkSensors()){	// Check needful sensors in device
				mode = "compass";
				changeView();
			} else {
				Toast.makeText(Game.this, "Device has not all needful sensors", Toast.LENGTH_SHORT);
			}			
			return (true);
		} else if (item.getItemId() == R.id.camera) { // Option VIDEO CAMERA - display video camera
			if (mode.equals("camera")){
				mode = "map";
				changeView();
			} else if (checkSensors() && checkCamera(this)){ // Check needful sensors in device and camera
				if (lastFix == null){
					Toast.makeText(Game.this, "No known location!", Toast.LENGTH_SHORT).show();
				} else if (checkCurrentLocation(lastFix, track.getPoint(searchingControl), distanceToControl)){
					Toast.makeText(Game.this, "You don't need a hint!", Toast.LENGTH_SHORT)
					.show();
				} else {
					mode = "camera";
					changeView();
				}				
			} else {
				Toast.makeText(Game.this, "Device has not all needful sensors or camera", Toast.LENGTH_SHORT)
					.show();
			}
			return (true);
		} else if (item.getItemId() == R.id.info){ // Option INFO
			if (mode.equals("info")){
				mode = "map";
			} else {
				mode = "info";
			}
			changeView();					
			return(true);
		} else if (item.getItemId() == R.id.settings){ // Option SETTINGS
			mode = "settings";
			changeView();		
			startActivity(new Intent(this, EditPreferences.class));
			return (true);
		} else if (item.getItemId() == R.id.lock_screen){ // Option LOCK SCREEN 
			if (item.getTitle().equals("LOCK")){
				item.setTitle("UNLOCK");
				item.setIcon(R.drawable.ic_menu_unlock);
				map.setClickable(false);
				map.setBuiltInZoomControls(false);
			} else {
				item.setTitle("LOCK");
				item.setIcon(R.drawable.ic_menu_lock);
				map.setClickable(true);
				map.setBuiltInZoomControls(true);
			}
		}
		return (super.onOptionsItemSelected(item));
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {		
		if (flagStart){
			// Enabled camera mode
			menu.findItem(R.id.camera).setEnabled(true);
		}
		if (flagFinish){
			// Disabled all modes
			menu.findItem(R.id.map).setEnabled(false);
			menu.findItem(R.id.info).setEnabled(false);
			menu.findItem(R.id.compass).setEnabled(false);
			menu.findItem(R.id.camera).setEnabled(false);
			menu.findItem(R.id.settings).setEnabled(false);
			menu.findItem(R.id.lock_screen).setEnabled(false);
		}
		return (super.onPrepareOptionsMenu(menu));
	}
	
	/** Method for PREFERENCES */
	private SharedPreferences.OnSharedPreferenceChangeListener prefListener=
			new SharedPreferences.OnSharedPreferenceChangeListener() {
		public void onSharedPreferenceChanged(SharedPreferences sharedPrefs, String key) {		
			if (key.equals("map_type")) {				
				setMap();
			}
			if (key.equals("location")){ // Provider for current location
				setLocationProvider();
			}
			if (key.equals("toleranc")){				
				tolerance = Integer.parseInt(prefs.getString("toleranc", "0"));	
				if (tolerance > 100){
					Toast.makeText(Game.this, "Maximal possible tolerance is 100m!", Toast.LENGTH_SHORT).show();	
					tolerance = 0;
				}
				tv_tolerance.setText(String.valueOf(tolerance) + " meters");				
			}
			if (key.equals("current_location")){ // Displaying of current location in front of control - yes/no
				if (prefs.getBoolean("current_location", false)){
					tv_currentLocation.setText(String.valueOf(distanceToControl) + " meters in front of the control");
				} else {
					tv_currentLocation.setText("NO - never display current location");
					distanceToControl = 0;
					removeCurrentLocation();
				}
			}
			if (key.equals("cl_distance")){
				distanceToControl = Float.parseFloat(prefs.getString("cl_distance", "0"));			
				if (distanceToControl > 500){
					Toast.makeText(Game.this, "Maximal distance for displaying of possition is 500m!", Toast.LENGTH_SHORT).show();		
					distanceToControl = 0;
					removeCurrentLocation();
				}
				tv_currentLocation.setText(String.valueOf(distanceToControl) + " meters in front of the control");
			}
		}
	};
}