package apt.orienteering;

import java.io.File;
import org.mapsforge.android.maps.MapActivity;
import org.mapsforge.android.maps.MapView;
import org.mapsforge.android.maps.mapgenerator.MapGenerator;
import org.mapsforge.android.maps.mapgenerator.tiledownloader.MapnikTileDownloader;
import org.mapsforge.core.GeoPoint;
import org.mapsforge.android.maps.overlay.ArrayItemizedOverlay;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

/** The class witch map for selection part of map */
public class CreateTrack extends MapActivity {
	/*
	 * 0 - after draw start
	 * 1 - after draw control
	 * 2 - after draw finish
	 * 3 - addItem next
	 * 4 - addItem back
	 * 5 - set mapGenerator - change type of map
	 */
	private boolean[] flag = new boolean[8];

	Track track = new Track();

	Button start = null;
	Button control = null;
	Button finish = null;
	
	MenuInflater contextMenu = null;
	ArrayItemizedOverlay itemizedOverlay = null;
	
	MapView map = null;
	MapGenerator mapG = null;	
	int flagMap = 0;
	File mapFile = null;
	
	private int fileLength = 0;

	/** The method at the creating this activity */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Set all flags to false
		for (int j = 0; j < 8; j++) {
			flag[j] = false;
		}		

		// Create intent for information from previous activity
		Intent intent = getIntent();
		// Take information from previous activity
		double lat = intent.getDoubleExtra("latitude", 0.0);
		double lon = intent.getDoubleExtra("longitude", 0.0);
		// New GeoPoint for center map on this
		GeoPoint point = new GeoPoint(lat, lon);
		// Set attributes of track
		track.setPath(intent.getStringExtra("path"));
		track.setPath(track.getPath() + intent.getStringExtra("fileName"));
		
		fileLength = intent.getIntExtra("length", 0);
		
		// Set content view for mapView with buttons for drawing track
		setContentView(R.layout.create_track);
		map = (MapView)findViewById(R.id.ct_mapview);
		start = (Button)findViewById(R.id.d_start);
		control = (Button)findViewById(R.id.d_control);
		finish = (Button)findViewById(R.id.d_finish);

		start.setOnClickListener(onDrawStart);
		control.setOnClickListener(onDrawControl);
		finish.setOnClickListener(onDrawFinish);

		// Set map file
		mapFile = new File(track.getPath());
		
		// Set map generator for off-line maps
		mapG = map.getMapGenerator();
		Log.i("map generator", String.valueOf(mapG));
		
		// Set possible type of maps (implicit Off-line maps)
		if ((mapFile.exists() && fileLength == 0) || (mapFile.exists() && mapFile.length() == fileLength)){
			// Off-line maps from saved file
			map.setMapFile(mapFile);
			mapG = map.getMapGenerator();
			Log.i("map generator", String.valueOf(mapG));
			flagMap = 0;
			flag[5] = true;
			// Set characters of mapView
			setting(point);
		} else if (Game.checkInternetConnection((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))){
			// On-line Mapnik maps
			MapnikTileDownloader mapDown = new MapnikTileDownloader();
			map.setMapGenerator(mapDown);
			flagMap = 1;
			flag[5] = false;
			// Set characters of mapView
			setting(point);			
		} else {// No saved file and no Internet connection
			Toast.makeText(this, "No saved file and no Internet connection - no possibility to display any map",
					Toast.LENGTH_SHORT).show();
			Intent i = new Intent(CreateTrack.this, AndroidOrienteering.class);
			CreateTrack.this.finish();
			i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			// Go back to main menu if is not possible to display map
			startActivity(i);
		}	
	}
	
	/** Setting characters of map view 
	 * @param point Point to center of map view */
	private void setting(GeoPoint point){
		// Setting character of mapView
		map.setClickable(true);
		map.getController().setCenter(point);
		map.setBuiltInZoomControls(true);
		map.getController().setZoom(14);
		map.setClickable(true);
		map.getMapZoomControls().setZoomLevelMax((byte) 17);
		map.getMapZoomControls().setZoomLevelMin((byte) 12);

		// Create the ItemizedOverlay and add the items - for catch of tapping on the map
		itemizedOverlay = new MyOverlay(getResources().getDrawable(R.drawable.ic_menu_exit), this);
		map.getOverlays().add(itemizedOverlay);

		Toast.makeText(this, "Use button START to draw a track", Toast.LENGTH_SHORT).show();
	}

	/** OnClick to button start for drawing start point method */
	private View.OnClickListener onDrawStart = new View.OnClickListener() {
		public void onClick(View v) {
			if (!start.isSelected()) {
				Toast.makeText(CreateTrack.this, "Select position of start point", Toast.LENGTH_SHORT)
					 .show();
				start.setSelected(true);
			} else {
				// Start button is selected - user can select position of start point
				start.setSelected(false);
			}
		}
	};

	/** OnClick to button control for drawing control points method */
	private View.OnClickListener onDrawControl = new View.OnClickListener() {
		public void onClick(View v) {
			if (!control.isSelected()) {
				Toast.makeText(CreateTrack.this, "Select position of control points", Toast.LENGTH_SHORT)
					 .show();
				control.setSelected(true);
				finish.setSelected(false);
			} else {
				// Control button is selected - user can select position of controls
				control.setSelected(false);
			}
		}
	};

	/** OnClick to button finish for drawing finish point method */
	private View.OnClickListener onDrawFinish = new View.OnClickListener() {
		public void onClick(View v) {
			if (!finish.isSelected()) {
				Toast.makeText(CreateTrack.this, "Select position of finish point", Toast.LENGTH_SHORT)
					 .show();
				control.setSelected(false);
				finish.setSelected(true);
			} else {
				// Finish button is selected - user can select position of finish point
				finish.setSelected(false);
			}
		}
	};
	
	/** Class for drawing Overlay */
	private class MyOverlay extends ArrayItemizedOverlay {
		MyOverlay(Drawable defaultMarker, Context context) {
			super(defaultMarker);
		}

		/** Function for tapping on the map view */
		@Override
		public boolean onTap(GeoPoint point, MapView map) {
			if (start.isSelected()) { // Draw start point
				// Set buttons
				start.setSelected(false);
				start.setEnabled(false);
				control.setEnabled(true);

				// Set flags
				flag[0] = true;
				flag[4] = true;

				track.setStart(point.getLatitude(), point.getLongitude());
				map.getOverlays().add(track.drawStart());
			}

			if (finish.isSelected()) { // Select and draw finish point
				// Check minimal distance between controls
				if (testDistanceBetweenControls(point, track.getPoint(track.getCountControls()-1))){
	
					track.setFinish(point.getLatitude(), point.getLongitude());
					// Draw point and way between points
					map.getOverlays().add(track.drawFinish());
					map.getOverlays().add(track.drawWayBetween2Points(track.getCountControls() - 1, track.getFinish()));
					// Set flags
					flag[2] = true;
					flag[1] = false;

					// Set buttons
					finish.setSelected(false);
					control.setEnabled(false);
					finish.setEnabled(false);

					// New Activity (content) for setting details about track andsaving the track
					nextActivity();
				}
			}

			if (control.isSelected()) { // Select and draq control point
				boolean checkDistance;
				
				if (track.getCountControls() == 0){ // Distance from start to 1. control
					checkDistance = testDistanceBetweenControls(point, track.getStart());
				} else { // Distance between controls
					checkDistance = testDistanceBetweenControls(point, track.getPoint(track.getCountControls()-1));
				}
				
				if (checkDistance){
					// Set flags
					flag[1] = true;
					flag[0] = false;

					// Draw point
					track.addPoint(track.getCountControls(), point);
					map.getOverlays().add(track.drawControl(track.getCountControls()));

					if (track.getCountControls() == 0) { // First control
						// Set button finish
						finish.setEnabled(true);

						// Draw way between controls
						map.getOverlays().add(track.drawWayBetween2Points(track.getCountControls(), track.getStart()));						
					} else { // Other controls
						// Draw way between controls
						map.getOverlays().add(track.drawWayBetween2Points(track.getCountControls(),	track.getCountControls() - 1));
					}

					track.setCountControls(track.getCountControls() + 1);

					if (track.getCountControls() == 30) { // Maximal 30 controls					
						control.setSelected(false);
						control.setEnabled(false);
						flag[1] = false;
					}
				}
			}
			return true;
		}
	}
	
	/** Function for control of distance between points - minimal distance must be 100 meters. */
	private boolean testDistanceBetweenControls(GeoPoint point1, GeoPoint point2){
		Location loc = new Location("");
		loc.setLatitude(point1.getLatitude());
		loc.setLongitude(point1.getLongitude());
		
		Location loc2 = new Location("");
		loc2.setLatitude(point2.getLatitude());
		loc2.setLongitude(point2.getLongitude());
		
		if (loc.distanceTo(loc2) < 100){
			Toast.makeText(CreateTrack.this, "Distance between 2 controls must be more than 100 meters", Toast.LENGTH_SHORT)
				 .show();
			return false;
		}		
		return true;
	}
	
	/** Function for starting next activity */
	private void nextActivity() {
		Intent i = new Intent(CreateTrack.this, SaveTrack.class);
		i.putExtra("startLat", track.getStart().getLatitude());
		i.putExtra("startLon", track.getStart().getLongitude());
		i.putExtra("finishLat", track.getFinish().getLatitude());
		i.putExtra("finishLon", track.getFinish().getLongitude());
		i.putExtra("countControl", track.getCountControls());
		i.putExtra("state", track.getPath());

		// Handing over controls
		for (int j = 0; j < track.getCountControls(); j++) {
			i.putExtra("controlLat" + j, track.getPoint(j).getLatitude());
			i.putExtra("controlLon" + j, track.getPoint(j).getLongitude());
		}

		startActivity(i);
	}
	
	/** ShowDialogMaps method
	 * This method displays a dialog for selection of type of maps. */
	private void showDialogMaps(){
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Select type of map");
		builder.setSingleChoiceItems(R.array.maps, flagMap, new DialogInterface.OnClickListener() {
		    public void onClick(DialogInterface dialog, int item) {
		    	dialog.cancel();
		        setMap(item);
		    }
		})
		.show();
	}
	
	/** Set type of map function */
	private void setMap(int item){		
		if (item == 0){
			// Off-line maps
			if ((mapFile.exists() && fileLength == 0) || (mapFile.exists() && mapFile.length() == fileLength)){
				if (!flag[5]){
					Log.i("map generator set map", String.valueOf(mapG));
					map.setMapGenerator(mapG);
					map.setMapFile(mapFile);
					flag[5] = true;
					flagMap = 0;
				}
			} else {
				Toast.makeText(this, "No saved map file, use other type of maps", Toast.LENGTH_SHORT).show();
			}				
		} else if (item == 1){	
			// On-line Mapnik maps
			if (Game.checkInternetConnection((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))){
				if(flag[5]){
					MapnikTileDownloader mapDown = new MapnikTileDownloader();
					map.setMapGenerator(mapDown);
					flag[5] = false;
					flagMap = 1;
				}
			} else {
				Toast.makeText(this, "No Internet connection", Toast.LENGTH_SHORT).show();
			}
		}
	}

	/** The method for showing closing dialog */
	private void showExitDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("Do you really want to go to main menu? Drawn track will be deleted!")
				.setCancelable(false)
				.setPositiveButton("YES", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// Go back to menu
						Intent i = new Intent(CreateTrack.this, AndroidOrienteering.class);
						CreateTrack.this.finish();
						i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(i);
					}
				})
				.setNegativeButton("NO", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				}).show();
	}
	
	/** On key button */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) { // Button BACK
			// Delete last drawn point
			if (flag[0]) {
				// Delete start point
				map.getOverlays().add(track.removeStart());
				flag[0] = false;
				flag[4] = false;

				finish.setEnabled(false);
				finish.setSelected(false);
				control.setEnabled(false);
				control.setSelected(false);

				start.setEnabled(true);
				return true;
			}
			if (flag[1]) {
				// Delete control point
				track.setCountControls(track.getCountControls() - 1);
				map.getOverlays().add(track.removeControl(track.getCountControls()));
				map.getOverlays().add(track.deleteWayBetween2Points());

				if (track.getCountControls() == 0) {
					// No more control points on the map
					flag[1] = false;
					flag[0] = true;					
					finish.setEnabled(false);
				}
				return true;
			} 
			if (flag[2]) {
				// Delete finish
				map.getOverlays().add(track.removeFinish());
				map.getOverlays().add(track.deleteWayBetween2Points());
				flag[2] = false;
				flag[1] = true;
				finish.setEnabled(true);
				control.setEnabled(true);
				return true;
			}
		}
		return super.onKeyDown(keyCode, event);
	}

	/** OPTIONS MENU methods */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		new MenuInflater(this).inflate(R.menu.option_create_track, menu);
		menu.removeItem(R.id.next_map);
		return (super.onCreateOptionsMenu(menu));
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.ct_settings) {
			// Set type of maps			
			showDialogMaps();			
			return (true);
		} else if (item.getItemId() == R.id.next_map) {
			nextActivity();
		} else if (item.getItemId() == R.id.menu){
			showExitDialog();
		}
		return (super.onOptionsItemSelected(item));
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// Add button NEXT
		if (flag[2] && !flag[3]) {
			// Finish is drawn and NEXT is not in the options menu 
			menu.add(0, R.id.next_map, 0, "NEXT");
			flag[3] = true;
		} else if (flag[3] && !flag[2]) {
			// NEXT is in the options menu and finish is not drawn
			menu.removeItem(R.id.next_map);
			flag[3] = false;
		}
		return (super.onPrepareOptionsMenu(menu));
	}
}