package apt.orienteering;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class ChooseTrack extends ListActivity {
	// Helpful variables
	Cursor model = null;
	TrackAdapter adapter = null;
	TrackHelper helper = null;

	public final static String ID_EXTRA = "apt.orienteering._ID";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.choose_track);

		helper = new TrackHelper(this);
		initList();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		helper.close();
	}

	/** Function for onListItemClick - selection of track */
	@Override
	public void onListItemClick(ListView list, View view, int position, long id) {
		Intent i = new Intent(ChooseTrack.this, SavePlay.class);

		i.putExtra(ID_EXTRA, String.valueOf(id));
		startActivity(i);
	}

	/** The function for initialization list of tracks */
	private void initList() {
		if (model != null) {
			stopManagingCursor(model);
			model.close();
		}

		model = helper.getInfoTracks("name");
		startManagingCursor(model);
		adapter = new TrackAdapter(model);
		setListAdapter(adapter);
	}

	class TrackAdapter extends CursorAdapter {
		TrackAdapter(Cursor c) {
			super(ChooseTrack.this, c);
		}

		@Override
		public void bindView(View row, Context ctxt, Cursor c) {
			TrackHolder holder = (TrackHolder) row.getTag();
			holder.populateFrom(c, helper);
		}

		@Override
		public View newView(Context ctxt, Cursor c, ViewGroup parent) {
			LayoutInflater inflater = getLayoutInflater();
			View row = inflater.inflate(R.layout.item_choose_track, parent, false);
			TrackHolder holder = new TrackHolder(row);
			row.setTag(holder);
			return (row);
		}
	}

	static class TrackHolder {
		private TextView name = null;
		private TextView author = null;
		private TextView control = null;
		private TextView length = null;

		TrackHolder(View row) {
			name = (TextView) row.findViewById(R.id.ch_track);
			author = (TextView) row.findViewById(R.id.ch_author);
			control = (TextView) row.findViewById(R.id.ch_control);
			length = (TextView) row.findViewById(R.id.ch_length);
		}

		void populateFrom(Cursor c, TrackHelper helper) {
			name.setText(helper.getName(c));
			author.setText(helper.getAuthor(c));
			control.setText(String.valueOf(helper.getCountControl(c)) + " controls");
			length.setText(String.valueOf(helper.getLength(c)) + "m");
		}
	}
}