package apt.orienteering;

import java.io.File;
import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

public class TrackDetails extends TabActivity {
	// Helpful variables
	private String trackId = null;
	TrackHelper helper = null;
	Cursor model = null;
	RaceAdapter adapter = null;
	ListView list = null;
	TabHost.TabSpec spec = null;
	// Flags for tabs
	boolean tabMap = false;
	boolean tabDetails = false;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Set content view
		setContentView(R.layout.track_details);
		// TrackHelper
		helper = new TrackHelper(this);
		// Information from last activity
		trackId = getIntent().getStringExtra(ChooseTrack.ID_EXTRA);
		// List for races
		list = (ListView)findViewById(R.id.doneraces);		
		initList();
		// Set tab with list
		spec = getTabHost().newTabSpec("tag1");
		spec.setContent(R.id.doneraces);
		spec.setIndicator("List");
		getTabHost().addTab(spec);getTabHost().setCurrentTab(0);
		// On item click listener
		list.setOnItemClickListener(onListClick);
		Toast.makeText(TrackDetails.this, "Press item for details", Toast.LENGTH_SHORT).show();
		
	}
	
	@Override
	public void onDestroy(){
		super.onDestroy();
		helper.close();
	}
	
	
	/** Function for on item click listener */
	private AdapterView.OnItemClickListener onListClick = new AdapterView.OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {			
			// Cursor for click item
			model.moveToPosition(position);
			
			int raceId = helper.getId(model);
			int track_id = helper.getTrackId(model);
			
			if (tabDetails){ // Remove view if exists
				getTabHost().getTabWidget().removeView(getTabHost().getTabWidget().getChildTabViewAt(2));
				
				tabDetails = false;
			}

			// Add view (tab)
			spec = getTabHost().newTabSpec("tag2");
			spec.setIndicator("Details");
			// Information for new tab
			Intent i = new Intent(TrackDetails.this.getApplicationContext(), DetailsTabView.class);
			i.putExtra("trackId", track_id);
			i.putExtra("raceId", raceId);
			i.putExtra("position", position);

			spec.setContent(i);
			getTabHost().addTab(spec);
			tabDetails = true;
			
			if (checkMap(track_id)){ // If is possible to display map, add next tab with map
				if (tabMap){ // Delete tab (view), if exists
					getTabHost().getTabWidget().removeView(getTabHost().getTabWidget().getChildTabViewAt(2));
				}

				spec = getTabHost().newTabSpec("tag3");
				spec.setIndicator("Map");
				// Information for tab with map
				Intent intent = new Intent(TrackDetails.this.getApplicationContext(), MapTabView.class);
				intent.putExtra("trackId", track_id);
				intent.putExtra("raceId", raceId);

				spec.setContent(intent);
				getTabHost().addTab(spec);
				tabMap = true;
			}
			
			getTabHost().setCurrentTab(1);
		}
	};	
	
	/** Control possibility to display some type of maps */
	private boolean checkMap(int track_id){
		Cursor c = helper.getInfoForGame(String.valueOf(track_id));
		c.moveToFirst();
		File mapFile = new File(helper.getState(c));
		c.close();
		
		if (mapFile.exists() || Game.checkInternetConnection((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))){
			return true;
		} else {;
			return false;
		}
	}
	
	/** The function for initialization list of races */
	private void initList() {
		if (model != null) {
			stopManagingCursor(model);
			model.close();
		}

		model = helper.getRaces(trackId, "totalTime");
		startManagingCursor(model);
		adapter = new RaceAdapter(model);
		list.setAdapter(adapter);
	}
	
	class RaceAdapter extends CursorAdapter {
		RaceAdapter(Cursor c) {
			super(TrackDetails.this, c);
		}

		@Override
		public void bindView(View row, Context ctxt, Cursor c) {
			RaceHolder holder = (RaceHolder)row.getTag();
			holder.populateFrom(c, helper);
		}

		@Override
		public View newView(Context ctxt, Cursor c, ViewGroup parent) {
			LayoutInflater inflater = getLayoutInflater();
			View row = inflater.inflate(R.layout.item_race, parent, false);
			RaceHolder holder = new RaceHolder(row);

			row.setTag(holder);

			return (row);
		}
	}
	
	static class RaceHolder {
		private TextView order = null;
		private TextView name = null;
		private TextView time = null;

		RaceHolder(View row) {
			order = (TextView)row.findViewById(R.id.r_order);
			name = (TextView)row.findViewById(R.id.r_name);
			time = (TextView)row.findViewById(R.id.r_time);
		}

		void populateFrom(Cursor c, TrackHelper helper) {
			
			order.setText(String.valueOf(c.getPosition()+1) + ".");
			name.setText(helper.getRunner(c));
			time.setText(helper.getTotalTime(c));

		}
	}

	/** OPTIONS MENU */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		new MenuInflater(this).inflate(R.menu.option_track_details, menu);
		menu.removeItem(R.id.td_settings);
		return (super.onCreateOptionsMenu(menu));
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {	
		if (item.getItemId() == R.id.td_back) {
			// Option EXIT - to finish application (this activity)
			Intent intent = new Intent(TrackDetails.this, AndroidOrienteering.class);
			TrackDetails.this.finish();
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			return (true);
		} 
		return (super.onOptionsItemSelected(item));
	}
}