package apt.orienteering;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.mapsforge.core.GeoPoint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/** SQLiteOpenHelper for database */
public class TrackHelper extends SQLiteOpenHelper {
	/* WARNING - when is necessary to upgrade database and user press at first
	 * button choose track, all program can crash because in choose track
	 * the database is opened only for reading
	 */
	
	private static final String DATABASE_NAME = "tracklist.db";
	private static final int SCHEMA_VERSION = 4;

	/** Constructor */
	public TrackHelper(Context context) {
		super(context, DATABASE_NAME, null, SCHEMA_VERSION);
	}

	/**
	 * Function for creation of database
	 * @param db New SQLiteDatabase
	 */
	@Override
	public void onCreate(SQLiteDatabase db) {
		// Type boolean is like integer 0 - false, 1 - true
		db.beginTransaction();
		try {
			// Create tables
			db.execSQL("CREATE TABLE tracks (_id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, author TEXT, controls INTEGER, length INTEGER," +
					"startLat REAL, startLon REAL, finishLat REAL, finishLon REAL, winner TEXT, winnerTime TEXT, state TEXT, done INTEGER);");
			db.execSQL("CREATE TABLE checkpoints (_id INTEGER PRIMARY KEY AUTOINCREMENT, track_id INTEGER, lat REAL, lon REAL);");
			db.execSQL("CREATE TABLE races (_id INTEGER PRIMARY KEY AUTOINCREMENT, track_id INTEGER, runner TEXT, date TEXT, totalTime TEXT);");
			db.execSQL("CREATE TABLE gpslogs (_id INTEGER PRIMARY KEY AUTOINCREMENT, race_id INTEGER, lat REAL, lon REAL, timestamp TEXT, control INTEGER);");
			db.setTransactionSuccessful();
		} catch (SQLException e) {
			Log.e("Error creating tables", e.toString());
			throw e;
		} finally {
			db.endTransaction();
		}
	}

	/**
	 * Method for upgrade of database
	 * @param db SQLiteDatabase
	 * @param oldVersion Number of version in a device
	 * @param newVersion Number of actual version of database
	 */
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w("upgrade", "Upgrading database from version " + oldVersion
				+ " to " + newVersion + ", which will destroy all old data.");

		db.beginTransaction();
		try {
			// Drop tables
			db.execSQL("DROP TABLE IF EXISTS tracks;");
			db.execSQL("DROP TABLE IF EXISTS checkpoints;");
			db.execSQL("DROP TABLE IF EXISTS races;");
			db.execSQL("DROP TABLE IF EXISTS gpslogs;");
			db.setTransactionSuccessful();
			
		} catch (SQLException e) {
			Log.e("Error upgrading tables", e.toString());
			throw e;
		} finally {
			db.endTransaction();
		}
		// Create all database again
		onCreate(db);
	}

	/**
	 * Function for insertion of data to the table tracks.
	 * Parameters are values for the table.
	 * @param name
	 * @param author
	 * @param count
	 * @param length
	 * @param start
	 * @param finish
	 * @param state
	 */
	public void insertTrack(String name, String author, int count, int length,
							GeoPoint start, GeoPoint finish, String state) {
		// New ContentValues for insertion of data to database
		ContentValues cv = new ContentValues();

		cv.put("name", name);
		cv.put("author", author);
		cv.put("controls", count);
		cv.put("length", length);
		cv.put("startLat", start.getLatitude());
		cv.put("startLon", start.getLongitude());
		cv.put("finishLat", finish.getLatitude());
		cv.put("finishLon", finish.getLongitude());
		cv.put("winnerTime", "00:00:00");
		cv.put("state", state);
		// New track - no race for the track done
		cv.put("done", 0);

		// Write to database
		getWritableDatabase().insert("tracks", null, cv);
	}

	/**
	 * Function for insertion of data to the table check points
	 * @param track_id Track ID in which is this check point
	 * @param point GPS of check point
	 */
	public void insertCheckPoint(int track_id, GeoPoint point) {
		ContentValues cv = new ContentValues();

		cv.put("track_id", track_id);
		cv.put("lat", point.getLatitude());
		cv.put("lon", point.getLongitude());

		getWritableDatabase().insert("checkpoints", null, cv);
	}

	/**
	 * Function for creation of new race
	 * @param track_id Track ID where is race
	 * @param date
	 */
	public void createRace(int track_id, String date) {
		ContentValues cv = new ContentValues();

		cv.put("track_id", track_id);
		cv.put("date", date);

		getWritableDatabase().insert("races", null, cv);
	}

	/**
	 * Function for reposting of GPS logs
	 * @param id Race ID in which is this log
	 * @param lat GPS latitude of the GPS log
	 * @param lon GPS longitude of the GPS log
	 * @param time Current time
	 * @param control Flag, if is the check point
	 */
	public void insertGPSlog(int id, double lat, double lon, String time, int control) {
		ContentValues cv = new ContentValues();

		cv.put("race_id", id);
		cv.put("lat", lat);
		cv.put("lon", lon);
		cv.put("timestamp", time);
		cv.put("control", control);

		getWritableDatabase().insert("gpslogs", null, cv);
	}

	/** 
	 * Function for finding of next race ID 
	 * @return ID of race
	 */
	public int getRaceId() {
		Cursor c = getReadableDatabase().rawQuery(
				"SELECT _id, track_id, runner, date FROM races ORDER BY _id",
				null);
		c.moveToPosition(c.getCount() - 1);
		int id = getId(c);
		c.close();
		return id;
	}

	/** 
	 * Function for finding of next track ID 
	 * @return ID of track
	 */
	public int getTrackId(String name) {
		Cursor c = this.getInfoTracks("_id");
		c.moveToPosition(c.getCount() - 1);
		int id = getId(c);
		c.close();
		return (id);
	}

	/**
	 * Function for save information about race and to update information about
	 * track 
	 * @param race_id
	 * @param time Total time of race
	 * @param runner Name of runner
	 * @param track_id
	 */
	public void saveDoneRace(int race_id, String time, String runner, int track_id) {
		ContentValues cv = new ContentValues();
		cv.put("runner", runner);
		cv.put("totalTime", time);

		String[] args = { Integer.toString(race_id) };

		// Insert name of runner and total time to table race
		try {
			getWritableDatabase().update("races", cv, "_id=?", args);
		} catch (SQLException e) {
			Log.e("Error update table races", e.toString());
		}

		ContentValues track = new ContentValues();
		String[] argstrack = { Integer.toString(track_id) };
		// Check if the time is new winner time
		// Winner time - race time > 0 => new winner time
		String timeString = getWinnerTimeOfTrack(String.valueOf(track_id));
		Log.i("stringTime2", timeString);
		if (timeString.equalsIgnoreCase("00:00:00")
				|| (convertStringToDate(timeString).getTime() - convertStringToDate(
						time).getTime()) > 0) {
			track.put("winner", runner);
			track.put("winnerTime", time);
		}

		track.put("done", 1);

		// Update info about track
		try {
			getWritableDatabase().update("tracks", track, "_id=?", argstrack);
		} catch (SQLException e) {
			Log.e("Error update table tracks", e.toString());
		}
	}

	/**
	 * Function deleteRace remove race from database
	 * @param race_id
	 */
	public void deleteRace(int race_id) {
		String[] args = { Integer.toString(race_id) };
		try {
			getWritableDatabase().delete("races", "_id=?", args);
		} catch (SQLException e) {
			Log.e("Error delete from table races", e.toString());
		}
	}

	// Helpful variable
	SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");

	/**
	 *  Helpful function for converting type String to Date
	 * @param dateString String which will be converted
	 * @return Date in Date type
	 */
	private Date convertStringToDate(String dateString) {
		Date date;
		Log.i("dateString", dateString);
		try {
			date = df.parse(dateString);			
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
		return date;
	}

	/**
	 * Function for finding winner time of special track
	 * @param id ID of track
	 * @return Winner time in String format
	 */
	public String getWinnerTimeOfTrack(String id) {
		String[] args = { id };
		Cursor c = getReadableDatabase().rawQuery(
				"SELECT _id, winnerTime FROM tracks WHERE _ID=?", args);
		c.moveToFirst();
		String time = c.getString(1);
		c.close();
		return time;
	}
	
	public int getCountTrackName(String trackName){
		String[] args = {trackName};
		Cursor c = getReadableDatabase().rawQuery("SELECT _id, name FROM tracks WHERE name=?", args);
		int count = c.getCount();
		c.close();
		return count;
	}

	public Cursor getRaces(String id, String orderBy){
		String[] args = {id};
		return (getReadableDatabase().rawQuery("SELECT _id, track_id, runner, date, totalTime FROM races WHERE track_id=? ORDER BY " + orderBy, args));
	}
	
	public int getTrackId(Cursor c){
		return(c.getInt(1));
	}
	
	public String getRunner(Cursor c){
		return(c.getString(2));
	}
	
	public String getDate(Cursor c){
		return(c.getString(3));
	}
	
	public String getTotalTime(Cursor c){
		return(c.getString(4));
	}
	
	/**
	 * Function for select information about tracks 
	 * @param orderBy Name of record for ordering
	 * @return Cursor with record from database
	 */
	public Cursor getInfoTracks(String orderBy) {
		return (getReadableDatabase().rawQuery(
				"SELECT _id, name, author, controls, length FROM tracks ORDER BY "
						+ orderBy, null));
	}
	
	public Cursor getInfoTracks(int id) {
		String track_id = String.valueOf(id);
	
		String[] args = {track_id};
		return (getReadableDatabase().rawQuery(
				"SELECT _id, name, author, controls, length FROM tracks WHERE _id=?", args));
	}

	public int getId(Cursor c) {
		return (c.getInt(0));
	}

	public String getName(Cursor c) {
		return (c.getString(1));
	}

	public String getAuthor(Cursor c) {
		return (c.getString(2));
	}

	public int getCountControl(Cursor c) {
		return (c.getInt(3));
	}

	public int getLength(Cursor c) {
		return (c.getInt(4));
	}

	/**
	 * Function for getting all done tracks
	 * @return Cursor with information from database
	 */
	public Cursor getDoneTracks(String orderBy) {
		String[] args = { "1" };
		return (getReadableDatabase()
				.rawQuery(
						"SELECT _id, name, author, controls, length, winner, winnerTime FROM tracks WHERE done=? ORDER BY "
								+ orderBy, args));
	}

	public String getWinner(Cursor c) {
		return (c.getString(5));
	}

	public String getWinnerTime(Cursor c) {
		return (c.getString(6));
	}

	/**
	 * Function for getting GPS logs from database (table gpslogs)
	 * @param race_id
	 * @return Cursor with information from database
	 */
	public Cursor getGPSlogs(String race_id) {
		String[] args = { race_id };
		return (getReadableDatabase()
				.rawQuery(
						"SELECT _id, race_id, lat, lon, timestamp, control FROM gpslogs WHERE race_id=? ORDER BY _id",
						args));
	}

	public double getLat(Cursor c) {
		return (c.getDouble(2));
	}

	public double getLon(Cursor c) {
		return (c.getDouble(3));
	}

	public String getTimestamp(Cursor c) {
		return (c.getString(4));
	}

	public int getFlagControl(Cursor c) {
		return (c.getInt(5));
	}

	/**
	 * Function for getting important information for game
	 * @param id Track ID
	 * @return Cursor with information from database
	 */
	public Cursor getInfoForGame(String id) {
		String[] args = { id };
		return (getReadableDatabase()
				.rawQuery(
						"SELECT _id, startLat, startLon, state, finishLat, finishLon FROM tracks WHERE _ID=?",
						args));
	}

	public double getStartLat(Cursor c) {
		return (c.getDouble(1));
	}

	public double getStartLon(Cursor c) {
		return (c.getDouble(2));
	}

	public String getState(Cursor c) {
		return (c.getString(3));
	}

	public double getFinishLat(Cursor c) {
		return (c.getDouble(4));
	}

	public double getFinishLon(Cursor c) {
		return (c.getDouble(5));
	}

	/**
	 * Function for getting information about controls in track
	 * @param id_track
	 * @return Cursor with information from database
	 */
	public Cursor getControls(String id_track) {
		String[] args = { id_track };
		return (getReadableDatabase().rawQuery(
				"SELECT _id, lat, lon FROM checkpoints WHERE track_id=?", args));
	}

	public double getControlLat(Cursor c) {
		return (c.getDouble(1));
	}

	public double getControlLon(Cursor c) {
		return (c.getDouble(2));
	}
	
	/**
	 * Function for getting time on control points in race
	 * @param id_race Race ID
	 * @return Cursor with times on control points
	 */
	public Cursor getTimes(String id_race){
		String[] args = { id_race, "1" };
		return (getReadableDatabase().rawQuery("SELECT _id, race_id, timestamp, control FROM gpslogs WHERE race_id=?  AND control=?", args));
	}
	
	public String getTime(Cursor c){
		return (c.getString(2));
	}
	
	/**
	 * Function for getting finish time in race
	 * @param id_race Race ID
	 * @return Finish time in race
	 */
	public String getFinishTime(String id_race){
		String[] args = { id_race };
		Cursor c = getReadableDatabase().rawQuery("SELECT _id, race_id, timestamp, control FROM gpslogs WHERE race_id=?", args);
		c.moveToLast();
		String time = c.getString(2);
		c.close();
		return time;
	}
	
	/**
	 * Function for getting start time in race
	 * @param id_race
	 * @return Start time in race
	 */
	public String getStartTime(String id_race){
		String[] args = { id_race };
		Cursor c = getReadableDatabase().rawQuery("SELECT _id, race_id, timestamp FROM gpslogs WHERE race_id=?", args);
		c.moveToFirst();
		String time = c.getString(2);
		c.close();
		return time;
	}
}