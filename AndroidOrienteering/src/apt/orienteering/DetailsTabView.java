package apt.orienteering;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class DetailsTabView extends Activity {
	// Global variables
	private int trackId = -1;
	private int raceId = -1;
	private int position = -1;
	private int winnerRaceId = 0;
	
	TrackHelper helper = null;	
	Cursor times = null;
	TimesAdapter tadapter = null;
	
	TextView author = null;
	TextView controls = null;
	TextView length= null;
	TextView runner = null;
	TextView date = null;
	TextView place = null;
	TextView totalTime = null;
	TextView winnerTime = null;
	TextView lostTime = null;
	ListView listTimes = null;

	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		
		// Set content view and values for items in view
		setContentView(R.layout.details_tab_view);
		author = (TextView)findViewById(R.id.author_name);
		controls = (TextView)findViewById(R.id.count_controls);
		length = (TextView)findViewById(R.id.length_td);
		runner = (TextView)findViewById(R.id.runner_name);
		date = (TextView)findViewById(R.id.date);
		place = (TextView)findViewById(R.id.place);
		totalTime = (TextView)findViewById(R.id.total_time_td);
		winnerTime = (TextView)findViewById(R.id.winner_time_td);
		lostTime = (TextView)findViewById(R.id.lost_time_td);		
		listTimes = (ListView)findViewById(R.id.time_between_controls);
		
		// TrackHelper
		helper = new TrackHelper(this);
		
		// Values from TrackDetails
		trackId = getIntent().getIntExtra("trackId", -1);		
		raceId = getIntent().getIntExtra("raceId", -1);
		position = getIntent().getIntExtra("position", -1);
		
		place.setText(String.valueOf(position));
		
		// Set information about track
		Cursor c = helper.getInfoTracks(trackId);
		c.moveToFirst();
		
		author.setText(helper.getAuthor(c));
		controls.setText(String.valueOf(helper.getCountControl(c)));
		length.setText(String.valueOf(helper.getLength(c)));
		winnerTime.setText(helper.getWinnerTimeOfTrack(String.valueOf(trackId)));
		
		c.close();

		// Set information about race
		c = helper.getRaces(String.valueOf(trackId), "totalTime");
		c.moveToPosition(position);
		winnerRaceId = helper.getId(c);
		runner.setText(helper.getRunner(c));		
		displayDate(c);
		place.setText(String.valueOf(c.getPosition()+1));
		totalTime.setText(helper.getTotalTime(c));
		countLostTime(raceId);
		// Set list with times between control points
		initListTimeBetweenControls(raceId);
		
		c.close();
	}
	
	@Override
	public void onDestroy(){
		super.onDestroy();
		helper.close();
	}
	
	/** Function displayDate - Display only date from timestamp */
	private void displayDate(Cursor c){
		String strDate = helper.getDate(c);
		
		String[] dateItems = new String[3];
		dateItems = strDate.split(" ");
		strDate = dateItems[0];
		dateItems = strDate.split("-");
		strDate = dateItems[2] + "." + dateItems[1] + "." + dateItems[0];
		// Display date in view
		date.setText(strDate);
	}
	
	/** Count and display lost time from total time to winner time */
	private void countLostTime(int raceId){
		Cursor c = helper.getGPSlogs(String.valueOf(raceId));
		Cursor cc = helper.getGPSlogs(String.valueOf(winnerRaceId));
		c.moveToFirst();
		// Timestamp from start point
		Date startDate = convertStringToLongDate(helper.getTimestamp(c));		
		c.moveToLast();
		// Timestamp from finish point
		Date finishDate = convertStringToLongDate(helper.getTimestamp(c));
		
		// Count total time from timestamps
		Date totalTime = new Date(finishDate.getTime() - startDate.getTime());
		
		// Count winner total time
		cc.moveToFirst();
		Date startDateW = convertStringToLongDate(helper.getTimestamp(cc));		
		cc.moveToLast();
		Date finishDateW = convertStringToLongDate(helper.getTimestamp(cc));		
		Date totalTimeW = new Date(finishDateW.getTime() - startDateW.getTime());
		// Count lost time
		Date lTime = new Date(totalTime.getTime() - totalTimeW.getTime());
		
		String stringTime = Game.milisecondsToDateString(lTime.getTime());
		
		lostTime.setText(stringTime);
		
		c.close();
		cc.close();
	}

	/** Set list with times between controls */
	private void initListTimeBetweenControls(int raceId) {		
		if (times != null) {
			stopManagingCursor(times);
			times.close();
		}

		times = helper.getTimes(String.valueOf(raceId));
		startManagingCursor(times);
		tadapter = new TimesAdapter(times);
		listTimes.setAdapter(tadapter);
	}
	
	class TimesAdapter extends CursorAdapter {
		TimesAdapter(Cursor c) {
			super(DetailsTabView.this, c);
		}

		@Override
		public void bindView(View row, Context ctxt, Cursor c) {
			TimesHolder holder = (TimesHolder)row.getTag();
			holder.populateFrom(c, helper, df, raceId);
		}

		@Override
		public View newView(Context ctxt, Cursor c, ViewGroup parent) {
			LayoutInflater inflater = getLayoutInflater();
			View row = inflater.inflate(R.layout.item_time, parent, false);
			TimesHolder holder = new TimesHolder(row);
			row.setTag(holder);
			return (row);
		}
	}
	
	static class TimesHolder {
		private TextView tv_control = null;
		private TextView tv_time = null;

		TimesHolder(View row) {
			tv_control = (TextView)row.findViewById(R.id.t_control);
			tv_time = (TextView)row.findViewById(R.id.t_time);
		}

		void populateFrom(Cursor c, TrackHelper helper, SimpleDateFormat df, int raceId) {
			if (c.getPosition()+1 == c.getCount()){ // Last point (finish)
				tv_control.setText("Finish");
			} else {
				tv_control.setText(String.valueOf(c.getPosition()+1) + ".");
			}
			
			Date time = null;
			try {
				time = df.parse(helper.getTime(c));
			} catch (ParseException e){
				e.printStackTrace();
			}
			
			Date lastTime = null;
			if (c.getPosition() == 0){
				Cursor cc = helper.getGPSlogs(String.valueOf(raceId));
				cc.moveToFirst();
				try {
					lastTime = df.parse(helper.getTimestamp(cc));
				} catch (ParseException e){
					e.printStackTrace();
				}
				cc.close();
			} else {
				c.moveToPrevious();
				try {
					lastTime = df.parse(helper.getTime(c));
				} catch (ParseException e){
					e.printStackTrace();
				}			
			}
			// Count time between 2 controls
			Date totalTime = new Date(time.getTime() - lastTime.getTime());
			
			String stringTime = Game.milisecondsToDateString(totalTime.getTime());			
			tv_time.setText(stringTime);
		}
	}

	// Helpful variable
	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSZ");
	/** Convert string to date type */
	private Date convertStringToLongDate(String dateString) {
		Date date;
		try {
			date = df.parse(dateString);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
		return date;
	}
}